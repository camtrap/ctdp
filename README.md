
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Package ctdp

ctdp - an R package to load and process camera-trap data in
[camtrap-dp](https://tdwg.github.io/camtrap-dp/) format

# Documentation

Documentation is being worked on, for now see
[here](https://wec.wur.nl/r/ctdp/).

Or, render the tutorial yourself via:

``` r
write_tutorial(camsample)
```
