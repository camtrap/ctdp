% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/ctdp_cut_gaps.r
\name{cut_gaps}
\alias{cut_gaps}
\title{Cut long gaps in deployments}
\usage{
cut_gaps(x, dtThreshold = 14, rmSingledep = TRUE)
}
\arguments{
\item{x}{an object of class \code{\link{ctdp}}}

\item{dtThreshold}{number of days, defaults to 14. An interval between consequtive sequences longer than this will break a deployment into separate parts.}

\item{rmSingledep}{\code{logical} defaults to TRUE: indicating whether or not deployments that consist of a single sequence should be removed}
}
\value{
an object of class \code{\link{ctdp}}
}
\description{
Cut long gaps in deployments
}
\examples{
NULL
}
\author{
Henjo de Knegt
}
