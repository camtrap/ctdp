#' @title Argument check on by
#' @inheritParams ctdp
#' @inheritParams by_void
#' @return same as \code{x} or an error
#' @author Henjo de Knegt
#' @seealso NULL
#' @keywords internal
#' @examples \dontrun{
#' by_argcheck(camsample, by = c("locationName","array"))
#' }
#' @export
by_argcheck <- function(x, by) {
  # input checks
  if(missing(x)) { stop("supply input x")}
  if(missing(by)) { stop("supply input by")}

  # Argument check
  by <- match.arg(by, choices = unlist(column_names(x, verbose = FALSE), recursive = TRUE, use.names = FALSE), several.ok = TRUE)
  
  # return
  return(by)
}