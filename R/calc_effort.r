#' Calculate effort
#' @param x an object of class \code{\link{ctdp}}
#' @inheritParams filter_timerange
#' @param subset optional expression for subsetting passed on to \code{\link{filter_station}}
#' @param groupBy an optional \code{character} vector with the column name(s) of the grouping variables (from the locations and deployments tables)
#' @inheritParams filter_timerange
#' @inheritParams filter_station
#' @inheritParams by_void
#' @return a \code{tibble} with columns specified in \code{by} and column "effort", which holds the total camera-days effort.
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}, \code{\link{filter_station}}
#' @examples \dontrun{
#' calc_effort(camsample)
#' calc_effort(camsample, by = "locationName")
#' calc_effort(camsample, start = "2017/4/1", end = "2017/8/1")
#' calc_effort(camsample, by = "locationName", subset = str_sub(locationName, 5, 5) == "3")
#' }
#' @export
calc_effort <- function(x, 
                        start = NULL, end = NULL, orders = "%Y/%m/%d", # filter_timerange
                        subset = NULL, # filter_station
                        by = NULL # by_void
                        ) {
  # checks
  if(missing(x)){stop("provide input x")}

  # filter time range?
  x <- filter_timerange(x, start = start, end = end, orders = orders)
  
  # filter on station?
  argList <- as.list(match.call(expand.dots = FALSE))[c("x","subset")]
  if(!is.null(argList$subset)){
    x <- do.call(filter_station,
                 argList,
                 quote = FALSE,
                 envir = parent.frame(n = 1L))
  }
  
  # Force interval
  x <- ctdp_interval(x)
  
  # get deployment info
  xdep <- x$deployments %>% 
    left_join(x$locations, by = "locationID") %>% 
    mutate(dep_length = int_length(deployment_interval) / 60 / 60 / 24) %>%  # DAYS
    select(-deployment_interval)
  
  # Grouped tibble?
  if(!is.null(by)){
    xdep <- xdep %>% 
      group_by(pick(all_of(by)))
  }
    
  # Sum effort over the groups
  xdepEffort <- xdep %>% 
    summarise(effort = sum(dep_length),
              .groups = "drop")
  
  # Return
  return(xdepEffort)
}