#' Specify species
#' @param species an optional \code{character} vector with name of the species (e.g. "Ocelot") to focus on, defaults to \code{NULL}
#' @return NULL
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @keywords internal
#' @export
species_void <- function(species = NULL) {
  NULL
}