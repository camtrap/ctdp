#' Internal function to apply filter expression
#' @param x a data.frame or tibble
#' @inheritParams filter_void
#' @return NULL
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' filter_apply(taxonomy(camsample), class == "Mammalia")
#' filter_apply(locations(camsample), str_sub(locationName, 5, 5) == "3")
#' }
#' @keywords internal
#' @export
filter_apply <- function(x, filter = NULL) {
  dofilter <- match.call(expand.dots = FALSE)$filter
  if(!is.null(dofilter)){
    r <- eval(dofilter, x, parent.frame(1L))
    if(!is.logical(r)) { stop("'filter' must be logical") }
    r <- r & !is.na(r)
    x <- x[r, , drop=FALSE]
  }
  return(x)
}