#' Retrieve the taxonomic species
#' @inheritParams ctdp
#' @return a \code{character} vector with the taxonomic species present in \code{x}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' taxon_species(camsample)
#' }
#' @export
taxon_species <- function(x) {
  y <- x$taxonomy %>% 
    count(vernacularNames.eng) %>% 
    drop_na() %>% 
    pull(vernacularNames.eng)
  return(y)
}