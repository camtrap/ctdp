#' Retrieve the taxonomic classes
#' @inheritParams ctdp
#' @return a \code{character} vector with the taxonomic classes present in \code{x}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' taxon_classes(camsample)
#' }
#' @export
taxon_classes <- function(x) {
  y <- x$taxonomy %>% 
    count(class) %>% 
    drop_na() %>% 
    pull(class)
  return(y)
}