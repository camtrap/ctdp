#' @title Argument check on order
#' @inheritParams ctdp
#' @inheritParams order_void
#' @return same as \code{x} or an error
#' @author Henjo de Knegt
#' @seealso \code{\link{taxon_orders}}
#' @keywords internal
#' @examples \dontrun{
#' argcheck_order(camsample, order = "Primates")
#' }
#' @export
argcheck_order <- function(x, order) {
  # input checks
  if(missing(x)) { stop("supply input x")}
  if(missing(order)) { stop("supply input order")}

  # Argument check
  order <- match.arg(order, choices = taxon_orders(x), several.ok = TRUE)
  
  # return
  return(order)
}