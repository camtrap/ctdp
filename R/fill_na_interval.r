#' @title Fill the NAs in deployment interval using sequence info
#' @description Fill the NAs in deployment interval using sequence info
#' @details NULL
#' @param x a \code{ctdp} object
#' @return a \code{ctdp} object
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @export
fill_na_interval <- function(x) {
  # input checks
  if(is_ctdp(x) == FALSE) { stop("x should be of class ctdp")}

  # Check whether there are NAs in any of the deployment intervals
  if(is.na(as.numeric(diff(time_range(x, based_on = "deployments"))))){
    # At least 1 NA somewhere
    dep_length <- map_dbl(x$deployments$deployment_interval, int_length)
    
    # Which deployment NA?
    dep_na <- which(is.na(dep_length))
    dep_naID <- x$deployments$deploymentID[dep_na]
    
    # Infer from sequences
    for(i in seq_along(dep_na)){
      # Get focal deployment
      iID <- dep_naID[i]
      
      # filter sequence info
      iSeq <- x$sequences %>% 
        select(deploymentID, sequence_interval) %>% 
        filter(deploymentID == iID)
      
      # time range sequences
      tmin <- min(int_start(iSeq$sequence_interval))
      tmax <- max(int_end(iSeq$sequence_interval))
      
      # Set as interval
      x$deployments$deployment_interval[dep_na[i]] <- interval(start = tmin, end = tmax)
    }
  }else{
    # No NAs
  }
  
  # Return
  return(x)
}