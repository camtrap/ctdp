#' Retrieve the taxonomy table of a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @inheritParams species_void
#' @return a \code{tibble} with the taxonomy table
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' taxonomy(camsample)
#' taxonomy(camsample, species = "lowland paca")
#' }
#' @export
taxonomy <- function(x, species = NULL) {
  y <- x$taxonomy
  if(!is.null(species)){
    species <- species_argcheck(x, species = species)
    y <- y %>% 
      filter(vernacularNames.eng %in% species)
  }
  return(y)
}