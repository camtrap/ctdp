#' Find the number of active cameras at a given set of times
#' @param x an object of class \code{\link{ctdp}}
#' @param t \code{POXIXct} or \code{character} timepoints for which to retrieve effort
#' @param orders \code{character} string with the orders passed on to \code{\link{parse_date_time}}
#' @return a plot, and silently it returns a tibble with effort data
#' @author Henjo de Knegt
#' @seealso \code{\link{plot_effort}}
#' @examples \dontrun{
#' point_effort(camsample, "2017/6/21 12:00:00")
#' }
#' @export
point_effort <- function(x, t, orders = "%Y/%m/%d %H:%M:%S") {
  # Get effort data
  effortData <- effort_table(x)
  
  # Get t if it is character
  if(is.character(t)){
    t <- parse_date_time(t, orders = orders, tz = x$settings$tz)
  }
  
  # Get interval
  i_t <- findInterval(t, effortData$time)
  
  # Return nr of active cams for each i_t
  return(effortData$nrCams[i_t])
}