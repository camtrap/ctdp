#' @title Check integrity of observation types
#' @description Check integrity of observation types
#' @details NULL
#' @param x a \code{ctdp} object
#' @return a named \code{list} with elements:
#' \itemize{
#'   \item \code{noNAs}: whether or not \code{observationType} column contains NAs
#'   \item \code{isFactor}: whether or not \code{observationType} columns is a factor
#'   \item \code{correctLevels}: whether or not the levels are one of: "animal","human","vehicle","blank","unknown","unclassified"
#' }
#' logical values indicating whether each table in \code{x} contains distinct (value TRUE) values or duplicates (value FALSE).
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' check_annotations(camsample)
#' }
#' @export
check_annotations <- function(x) {
  # input checks
  if(missing(x)) { stop("supply input x")}

  # Check annotation type
  y <- x$observations %>% count(observationType)

  # checks
  checkAnnotations <- list(
    noNAs = all(!is.na(y$observationType)),
    isFactor = is.factor(y$observationType),
    correctLevels = ifelse(is.factor(y$observationType),
                           all(levels(y$observationType) %in% c("animal","human","vehicle","blank","unknown","unclassified")),
                           all(y$observationType %in% c("animal","human","vehicle","blank","unknown","unclassified"))))

  # Return
  return(checkAnnotations)
}