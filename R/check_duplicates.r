#' @title Check presence of duplicates
#' @description Check presence of duplicates
#' @details NULL
#' @param x a \code{ctdp} object
#' @return a named \code{list} logical values indicating whether each table in \code{x} contains distinct (value TRUE) values or duplicates (value FALSE).
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' check_duplicates(camsample)
#' }
#' @export
check_duplicates <- function(x) {
  # input checks
  if(missing(x)) { stop("supply input x")}

  # store in list
  duplicateChecks <- list()
 
  # function to check duplication (of all rows/columns of a tibble)
  fnDuplicates <- function(y) {
    # check first based on first column, which is primary key column
    if(length(y[[1]]) == length(unique(y[[1]]))){
      return(TRUE)
    }else{
      # if first column is not unique, then proceed with unique entire tibble
      return(nrow(y) == nrow(unique(y)))
    }
  }
  
  for(i in c("locations","deployments","sequences","observations","media","taxonomy")) {
    duplicateChecks[[i]] <- fnDuplicates(x[[i]])
  }
  
  # Return
  return(duplicateChecks)
}