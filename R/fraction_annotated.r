#' Compute the fraction of sequences that is annotated
#' @param x an object of class \code{\link{ctdp}}
#' @param onlyMotion \code{logical}, whether or not to only use motion detection images, defaults to \code{TRUE}
#' @param omitSetupType \code{logical}, whether or not to omit sequences that are marked as some form of setup (`observations` column `cameraSetupType` is than not NA), defaults to \code{TRUE}
#' @inheritParams filter_timerange
#' @inheritParams filter_station
#' @inheritParams by_void
#' @return a tibble with summary data per deployment (or group specified in \code{by}), with columns:
#' \itemize{
#'   \item deploymentID, or columns specified in \code{by}
#'   \item fracAnnotated: the fraction of all sequences in this group that have been annotated (observationType != "unclassified")
#'   \item nrSeqs: number of sequences in this group (optionally after filter for only motion detection)
#'   \item nrSeqsAnnotated: number of annotated sequences in this group (optionally after filter for only motion detection)
#'   \item nrPhotos: number of photos in this group (optionally after filter for only motion detection)
#' }
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' fraction_annotated(camsample)
#' fraction_annotated(camsample, by = "locationName")
#' fraction_annotated(camsample, by = "transect")
#' fraction_annotated(camsample, start = "2017/4/1", end = "2017/8/1")
#' fraction_annotated(camsample, by = "locationName", subset = str_sub(locationName, 5, 5) == "3")
#' }
#' @export
fraction_annotated <- function(x,
                               onlyMotion = TRUE,
                               omitSetupType = TRUE,

                               start = NULL, end = NULL, orders = "%Y/%m/%d", # filter_timerange
                               subset = NULL, # filter_station
                               by = NULL # by_void
                               ) {
  # Collect arguments
  allArgs <- as.list(match.call(expand.dots = FALSE))
  
  # filter time range?
  x <- filter_timerange(x, start = start, end = end, orders = orders)
  
  # filter on station?
  if(!is.null(allArgs$subset)){
    x <- do.call(filter_station,
                 allArgs[c("x","subset")],
                 quote = FALSE,
                 envir = parent.frame(n = 1L))
  }
  
  # Get subset of data in big tibble
  y <- merge_tibbles(x, dropMedia = TRUE)
  keepCols <- c(by,
                "deploymentID", "sequenceID", "captureMethod", "observationType", "cameraSetupType", "nrphotos") %>% 
    unique()
  y <- y %>% 
    select(all_of(keepCols))
  
  # remove cameraSetupType != NA?
  if(omitSetupType){
    y <- y %>% 
      filter(is.na(cameraSetupType))
  }
  y <- y %>% 
    select(-cameraSetupType)
  
  # groupings; per sequence and per deployment
  groups1 <- c("deploymentID", "sequenceID")
  groups2 <- c("deploymentID")
  if(!is.null(by)){
    groups1 <- unique(c(by, groups1))
    groups2 <- unique(c(by, groups2))
  }
  
  # check whether or not annotated
  y <- y %>% 
    mutate(isAnnotated = observationType != "unclassified")
  
  # Only motion detections?
  if(onlyMotion){
    y <- y %>% 
      filter(captureMethod == "motion detection")
  }
  
  # Summarize per sequence
  y <- y %>%
    group_by(across(all_of(groups1))) %>% 
    summarize(nrPhotos = mean(nrphotos),
              isAnnotated = any(isAnnotated),
              .groups = "drop")
  
  # Summarize per deployment
  y <- y %>% 
    group_by(across(all_of(groups2))) %>% 
    summarize(nrSeqs = n(),
              nrSeqsAnnotated = sum(isAnnotated),
              nrPhotos = sum(nrPhotos),
              .groups = "drop")
  # y
  
  # relocate
  y <- y %>% 
    relocate(deploymentID, .before = nrSeqs)
  
  # Summarise on 'by'?
  if(!is.null(by)){
    y <- y %>% 
      group_by(across(all_of(by))) %>% 
      summarize(nrSeqs = sum(nrSeqs),
                nrSeqsAnnotated = sum(nrSeqsAnnotated),
                nrPhotos = sum(nrPhotos),
                .groups = "drop")
  }
  
  # Compute fraction
  y <- y %>% 
    mutate(fracAnnotated = nrSeqsAnnotated / nrSeqs) %>% 
    relocate(fracAnnotated, .before = nrSeqs)
  
  # return
  return(y)
}