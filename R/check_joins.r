#' @title Check data integrity of joins
#' @description Check data integrity of joins
#' @details NULL
#' @param x a \code{ctdp} object
#' @return a \code{list} with 4 elements:
#' \itemize{
#'   \item \code{allunique}: a \code{logical} indicating whether or not all primary key columns contain unique values
#'   \item \code{alljoins}: a \code{logical} indicating whether or not the tibbles have matching keys
#'   \item \code{unique}: a \code{tibble} with extra info on whether which element in \code{x} contains unique primary keys
#'   \item \code{anti_joins}: a named \code{list} with information on anti-joins (a character vector of length 0 if there are not anti-joins)
#' }
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' check_joins(camsample)
#' }
#' @export
check_joins <- function(x) {
  # input checks
  if(missing(x)) { stop("supply input x")}

  # Get key columns and link structure
  keys <- keycolNames()
  links <- linkStructure()
  
  # structure to hold data
  joinChecks <- list()
  
  ### Uniqueness of ID cols
  allUnique <- function(y){
    length(y) == length(unique(y))
  }
  joinChecks[["unique"]] <- tibble(
    locations = allUnique(x$locations$locationID),
    deployments = allUnique(x$deployments$deploymentID),
    sequences = allUnique(x$sequences$sequenceID),
    observations = allUnique(x$observations$observationID),
    media = allUnique(x$media$mediaID),
    taxonomy = allUnique(x$taxonomy$taxonID)
  )

  ### Anti-joins
  joinChecks[["anti_joins"]] <- list()
  
  fnNotin <- function(y, z, na.rm = TRUE) {
    if(na.rm){
      y <- y[!is.na(y)]
    }
    y <- unique(y)
    z <- unique(z)
    ynotinz <- y[! y %in% z]
    return(ynotinz)
  }
  # locs in deps
  joinChecks[["anti_joins"]][["loc_notin_dep"]] <- fnNotin(x$locations$locationID, x$deployments$locationID)
  joinChecks[["anti_joins"]][["dep_notin_loc"]] <- fnNotin(x$deployments$locationID, x$locations$locationID)

  # deps in seqs
  joinChecks[["anti_joins"]][["dep_notin_seq"]] <- fnNotin(x$deployments$deploymentID, x$sequences$deploymentID)
  joinChecks[["anti_joins"]][["seq_notin_dep"]] <- fnNotin(x$sequences$deploymentID, x$deployments$deploymentID)
  
  # seqs in obs
  joinChecks[["anti_joins"]][["seq_notin_obs"]] <- fnNotin(x$sequences$sequenceID, x$observations$sequenceID)
  joinChecks[["anti_joins"]][["obs_notin_seq"]] <- fnNotin(x$observations$sequenceID, x$sequences$sequenceID)
  
  # seqs in media
  if(x$settings$media) {
    joinChecks[["anti_joins"]][["seq_notin_med"]] <- fnNotin(x$sequences$sequenceID, x$media$sequenceID)
    joinChecks[["anti_joins"]][["med_notin_seq"]] <- fnNotin(x$media$sequenceID, x$sequences$sequenceID)
    
  }
  
  # taxonomy in observation 
  joinChecks[["anti_joins"]][["obs_notin_tax"]] <- fnNotin(x$observations$taxonID, x$taxonomy$taxonID)
  
  # Summarize and rearrange
  joinChecks[["alljoins"]] <- all(as.integer(unlist(lapply(joinChecks[["anti_joins"]], length))) == 0L)
  joinChecks[["allunique"]] <- all(as.logical(joinChecks[["unique"]][1,,drop=TRUE]))
  joinChecks <- joinChecks[c("allunique","alljoins","unique","anti_joins")]
  
  # Return
  return(joinChecks)
}