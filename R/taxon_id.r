#' Get the taxonID for a species
#' @param x an object of class \code{\link{ctdp}}
#' @return a \code{character} vector with the corresponding taxonID values
#' @author Henjo de Knegt
#' @seealso \code{\link{taxon_species}}
#' @examples \dontrun{
#' taxon_id(camsample)
#' taxon_id(camsample, spp = "lowland paca")
#' taxon_id(camsample, spp = "LowlandPaca")
#' taxon_id(camsample, spp = "_LowLand_ PaCa")
#' }
#' @export
taxon_id <- function(x, spp = NULL) {
  if(is.null(spp)){
    return(x$taxonomy$taxonID)
  }else{
    if(length(spp) > 1){
      ids <- sapply(spp, function(z){ taxon_id(x,z)}, USE.NAMES = FALSE)
      return(ids)
    }else
    {
      # Get taxonomy
      xtax <- x$taxonomy
      
      # Prep
      xtax_key <- xtax %>% select(taxonID)
      xtax_rest <- xtax %>% select(-taxonID)
      xtax_rest <- xtax_rest %>% 
        mutate(across(.cols=where(is.character),.fns = tolower),
               across(.cols=where(is.character),.fns = function(x){gsub(x, pattern=" ", replacement="")}))
      xtax_all <- bind_cols(xtax_key, xtax_rest)
      
      # Prep spp
      spp <- gsub(tolower(spp), pattern=" ", replacement="")
      spp <- gsub(tolower(spp), pattern= "-|_|", replacement="")
      
      
      ### Filter for ANY column (NOT taxonID) containing spp
      xtax_sub <- xtax_all %>% 
        rowwise() %>% 
        filter(any(c_across(-taxonID) == spp))
      
      ### Return taxonID
      return(xtax_sub$taxonID)
    }
  }
}