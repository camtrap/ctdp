#' @title Remove duplicate records from the tables of a ctdp object
#' @description Remove duplicate records from the tables of a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @return an object of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' distinct(camsample)
#' }
#' @describeIn ctdp Remove duplicate records from the tables of a ctdp object
#' @export
distinct.ctdp <- function(x) {
  for(i in names(keycolNames())) {
    x[[i]] <- x[[i]] %>% distinct()
  }
  
  # return
  return(x)
}