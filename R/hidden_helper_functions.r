#' Hidden helper functions
#' @param x \code{character} path, or an object of class \code{\link{ctdp}}
#' @return for \code{pathProperties} a \code{list} with folder/file information
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @keywords internal
#' @name helpers

#' @rdname helpers
#' @export
is.ctdp <- function(x) {
  y <- "ctdp" %in% class(x)
  return(y)
}

#' @rdname helpers
#' @export
pathProperties <- function(x) {
  # x is either ctdp object or character string of length 1
  if(is.ctdp(x)) {
    x <- x$settings$path
  }else{
    if(is.character(x)) {
      if(length(x) != 1) {
        stop("x should be either ctdp object, or character string of length 1")
      }
    }else{
      stop("x should be either ctdp object, or character string of length 1")
    }
  }
  
  # Get dirname and basename
  y <- list(path = dirname(x),
            fileFolder = basename(x))
  
  # Get extension
  ex <- strsplit(basename(x), split="\\.")[[1]]
  ex <- ex[length(ex)]
  
  # if same as basename, then no extension
  if(ex == basename(x)) { ex <- NA_character_ }
  
  # add info to list
  y[["extension"]] <- ex
  y[["isZip"]] <- (tolower(ex) == "zip")
  if(is.na(y[["isZip"]])){ y[["isZip"]] <- FALSE}  
  
  # return list y
  return(y)
}


#' @rdname helpers
#' @export
list2tbl <- function(x) {
  y <- lapply(x, function(y) { unlist(y) %>%
      as.list() %>%
      as_tibble()}) %>%
    bind_rows()
  return(y)
}


#' @rdname helpers
#' @export
dttmColNames <- function(x) {
  dttmCols <- list()
  doElements <- c("deployments","sequences","media","observations")
  doElements <- doElements[doElements %in% names(x)]
  for(i in doElements) {
    icols <- character()
    for(j in names(x[[i]])) {
      if(is.POSIXct(x[[i]][[j]])){
        icols <- c(icols, j)
      }
    }
    dttmCols[[i]] <- icols
  }
  return(dttmCols)
}


#' @rdname helpers
#' @export
intervalColNames <- function(x) {
  intervalCols <- list()
  doElements <- c("deployments","sequences","media","observations")
  doElements <- doElements[doElements %in% names(x)]
  for(i in doElements) {
    icols <- character()
    for(j in names(x[[i]])) {
      if(is.interval(x[[i]][[j]])){
        icols <- c(icols, j)
      }
    }
    intervalCols[[i]] <- icols
  }
  return(intervalCols)
}


#' @rdname helpers
#' @export
printTimeZones <- function(x) {
  cat("<dttm> columns:\n")
  dttm_colNames <- dttmColNames(x)
  for(i in names(dttm_colNames)) {
    for(j in dttm_colNames[[i]]) {
      cat(paste0(i, "$", j , ": ", attr(x[[i]][[j]][[1]], 'tzon'), "\n"))
    }  
  }
  
  cat("\n\n<Interval> columns:\n")
  interval_colNames <- intervalColNames(x)
  for(i in names(interval_colNames)) {
    for(j in interval_colNames[[i]]) {
      cat(paste0(i, "$", j , ": ", attr(x[[i]][[j]][[1]], 'tzon'), "\n"))
    }  
  }
}


#' @rdname helpers
#' @export
hardCodedColNames <- function() {
  # keycolNames()
  # linkStructure()
  
  # named list with named elements used for parsing names
  y <- list(
    # Deployments/locations
    deployments = c(locationID = "locationID", ### name of element is NEW name, value is camtrap-dp name
                    deploymentID = "deploymentID",
                    locationName = "locationName",
                    longitude = "longitude",
                    latitude = "latitude",
                    deployment_start = "start",
                    deployment_end = "end",
                    cameraID = "cameraID",
                    timestampIssues = "timestampIssues"),
    
    # Sequences/media
    media = c(deploymentID = "deploymentID",
              sequenceID = "sequenceID",
              mediaID = "mediaID",
              media_timestamp = "timestamp",
              captureMethod = "captureMethod",
              filePath = "filePath",
              fileName = "fileName",
              fileMediatype = "fileMediatype",
              favourite = "favourite"),
    
    # Observations
    observations = c(observationID = "observationID",
                     sequenceID = "sequenceID",
                     taxonID = "taxonID",
                     observation_timestamp = "classificationTimestamp",
                     observationType = "observationType",
                     classificationMethod = "classificationMethod",
                     count = "count",
                     cameraSetup = "cameraSetup"),
    
    # Taxonomy
    taxonomy = c(taxonID = "taxonID",
                 scientificName = "scientificName",
                 taxonRank = "taxonRank",
                 vernacularNames.eng = "vernacularNames.eng")) # ,
                 # vernacularNames.nld = "vernacularNames.nld"))
  
  # Return
  return(y)
}


#' @rdname helpers
#' @export
checkCodedColNames <- function(x) {
  # which names should it have?
  y <- hardCodedColNames()
  noMatch = list()
  
  # for each element in y, check the names
  for(iname in names(y)) {
    matchNames <- as.character(y[[iname]]) %in% names(x[[iname]])
    if(all(matchNames) == FALSE) {
      noMatch[[iname]] = y[[iname]][which(matchNames == FALSE)]
    }
  }
  
  # return list
  return(list(pass = length(noMatch) == 0,
              noMatch = noMatch))
}


#' @rdname helpers
#' @export
setCodedColNames <- function(x) {
  
  # Checks
  xchecks <- checkCodedColNames(x)
  if(xchecks$pass == FALSE) {
    stop("coded names do not pass check")
  }
  
  # named vector with recoding values
  y <- hardCodedColNames()
  
  # Make copy 
  z <- x
  
  # Recode using !!! notation
  for(i in names(y)) {
    inames <- names(z[[i]])
    setNms <- names(y[[i]])
    names(setNms) <- as.character(y[[i]])
    inames <- recode(inames, !!!setNms)
    names(z[[i]]) <- inames
  }
  
  # return
  return(z)
}


#' @rdname helpers
#' @export
keycolNames <- function() {
  y <- list(locations = "locationID",
            deployments = "deploymentID",
            sequences = "sequenceID",
            observations = "observationID",
            taxonomy = "taxonID",
            media = "mediaID")
  return(y)
}


#' @rdname helpers
#' @export
linkStructure <- function() {
  y <- list(locations = c("locations"),
            deployments = c("locations","deployments"),
            sequences = c("deployments","sequences"),
            observations = c("sequences","observations","taxonomy"),
            media = c("sequences","media"),
            taxonomy = c("taxonomy"))
  return(y)
}


#' @rdname helpers
#' @export
timestampCols <- function() {
  y <- list(deployments = c("deployment_start","deployment_end"),
            observations = "observation_timestamp",
            media = "media_timestamp")
  return(y)
}


#' @rdname helpers
#' @export
intervalCols <- function() {
  y <- list(deployments = "deployment_interval",
            sequences = "sequence_interval")
  return(y)
}


#' @rdname helpers
#' @export
removeEmptyCols <- function(x, keepMainCols = TRUE) {
  y <- x
  if(keepMainCols) {
    keepCols <- c(
      unlist(lapply(hardCodedColNames(), names), use.names = FALSE),
      unlist(keycolNames(), use.names = FALSE),
      unlist(timestampCols(), use.names = FALSE),
      unlist(intervalCols(), use.names = FALSE)
      ) %>% 
      unique() %>% 
      sort()
  }else{
    keepCols <- character(0)
  }
  
  for(i in seq_along(y)) {
    if(is_tibble(y[[i]])) {
      nrNAs <- colSums(is.na(y[[i]]))
      nrRows <- nrow(y[[i]])
      if(any(nrNAs == nrRows)) {
        rmWhich <- which(nrNAs == nrRows)
        rmWhich <- names(rmWhich)
        rmWhich <- rmWhich[! rmWhich %in% keepCols]
        if(length(rmWhich) > 0) {
          y[[i]] <- y[[i]] %>% 
            select(-all_of(rmWhich))
        }
      }
    }
  }
  
  return(y)
}




#' @rdname helpers
#' @export
intervalColNames <- function(x) {
  intCols <- list()
  for(i in c("deployments","sequences")) {
    icols <- character()
    for(j in names(x[[i]])) {
      if(is.interval(x[[i]][[j]])) {
        icols <- c(icols, j)
      }
    }
    intCols[[i]] <- icols
  }
  return(intCols)
}


#' @rdname helpers
#' @export
tableInfo <- function(x, table, verbose = TRUE) {
  table <- match.arg(table, names(x)[names(x) != "settings"])
  all_columns <- names(x[[table]])
  dttm_colNames <- dttmColNames(x)
  dttm_colNames <- dttm_colNames[[table]]
  int_colNames <- intervalColNames(x)
  int_colNames <- int_colNames[[table]]
  key_colNames <- keycolNames()
  key_main <- key_colNames[[table]]
  key_other <- as.character(unlist(key_colNames[-match(table, names(key_colNames))]))
  key_other <- key_other[key_other %in% all_columns]
  all_other_columns <- all_columns[! all_columns %in% c(dttm_colNames,
                                                        key_main,
                                                        key_other)]
  tblinfo <- list(key_main = key_main,
                  key_other = key_other,
                  dttm = dttm_colNames,
                  interval = int_colNames,
                  other = all_other_columns)
  if(verbose){ print(tblinfo) }
  invisible(tblinfo)
}


#' @rdname helpers
#' @export
duplicateNames <- function(x, keyNames = keycolNames()) {
  # x is output of read_camtrap_dp
  duplNameList <- list()
  for(i in names(x)){
    if(is.data.frame(x[[i]])){
      duplNameList[[i]] <- list()
      for(j in names(x)){
        if(i != j){
          if(is.data.frame(x[[j]]))
          {
            i_names <- names(x[[i]]); i_names <- i_names[! i_names %in% keyNames]
            j_names <- names(x[[j]]); j_names <- j_names[! j_names %in% keyNames]
            duplNameList[[i]][[j]] <- i_names[i_names %in% j_names]
          }
        }
      }
    }
  }
  return(duplNameList)
}


#' @rdname helpers
#' @export
forceUniqueNames <- function(x, keyNames = keycolNames()) {
  # Get duplicated column names
  x_duplicated <- duplicateNames(x, keyNames)
  
  # update names
  for(i in names(x)[-1]){
    renameCols <- x_duplicated[[i]] %>% 
      unlist() %>% 
      as.character() %>% 
      unique()
    names(x[[i]])[names(x[[i]]) %in% renameCols] <- paste(i,  names(x[[i]])[names(x[[i]]) %in% renameCols], sep="_")
  }
  
  # return
  return(x)
}
