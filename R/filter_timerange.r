#' Filter a ctdp object based on a specified time range
#' @details This function first filters sequences, where sequences are kept whose START falls inside the specified interval, and then filters the other elements to the sequences that are kept. Then, the sequence and deployment intervals are truncated to [start, end]
#' @param x an object of class \code{\link{ctdp}}
#' @param start \code{character} datetime: sequences are kept when the start of the sequences is > start, passed on to \code{\link{parse_date_time}}. Defaults to \code{NULL}
#' @param end \code{character} datetime: sequences are kept when the start of the sequences is < end, passed on to \code{\link{parse_date_time}}. Defaults to \code{NULL}
#' @param orders \code{character} string with the orders passed on to \code{\link{parse_date_time}}, defaults to "Ymd"
#' @return x an object of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#'   filter_timerange(x, start = "2021/6/21", end = "2022/6/21")
#' }
#' @export
filter_timerange <- function(x, start = NULL, end = NULL, orders = "%Y/%m/%d") {
  ### Checks
  if(is.null(start) & is.null(end)){
    return(x)
  }else {
    # Get start/end as datetime object in same timezone
    {
      if(! is.null(start)) {
        if(! is.character(start)) { stop("start should be of class character")}
        start <- parse_date_time(start, orders = orders, tz=x$settings$tz)
      }else {
        start <- min(int_start(x$sequences$sequence_interval))
      }
      if(! is.null(end)) {
        if(! is.character(end)) { stop("end should be of class character")}
        end <- parse_date_time(end, orders = orders, tz=x$settings$tz)
      }else {
        end <- max(int_end(x$sequences$sequence_interval))
      }
    }
    # start
    # end
    
    
    ### Force to interval interval
    wasInterval <- x$settings$interval
    x <- ctdp_interval(x)
    

    ### Subset to > start, < end
    {
      # FIRST: filter sequences: START of a sequence will be <INSIDE> the specified interval
      x$sequences <- x$sequences %>%
        filter(int_start(sequence_interval) > start,
               int_start(sequence_interval) < end)
      # x$sequences %>% pull(sequence_interval) %>% int_start() %>% min()
      # x$sequences %>% pull(sequence_interval) %>% int_start() %>% max()
      
      # THEN: filter the deployments, locations, observations and media accordingly
      x$deployments <- x$deployments %>%
        filter(deploymentID %in% x$sequences$deploymentID)
      x$locations <- x$locations %>%
        filter(locationID %in% x$deployments$locationID)
      x$observations <- x$observations %>%
        filter(sequenceID %in% x$sequences$sequenceID)
      x$media <- x$media %>%
        filter(sequenceID %in% x$sequences$sequenceID)
    }
    # x
    
    
    ### Truncate intervals to [start,end]
    {
      truncInterval <- lubridate::interval(start, end)
      x$deployments$deployment_interval <- lubridate::intersect(x$deployments$deployment_interval, truncInterval)
      x$sequences$sequence_interval <- lubridate::intersect(x$sequences$sequence_interval, truncInterval)
    }
    
    
    ### Reset interval if it was not
    if(wasInterval == FALSE) {
      x <- ctdp_interval(x, rev = TRUE)
    }
    

    ### return
    return(x)
  }
}