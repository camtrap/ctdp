#' @title Argument check on class
#' @inheritParams ctdp
#' @inheritParams class_void
#' @return same as \code{x} or an error
#' @author Henjo de Knegt
#' @seealso \code{\link{taxon_classes}}
#' @keywords internal
#' @examples \dontrun{
#' class_argcheck(camsample, class = "Mammalia")
#' }
#' @export
class_argcheck <- function(x, class) {
  # input checks
  if(missing(x)) { stop("supply input x")}
  if(missing(class)) { stop("supply input class")}

  # Argument check
  class <- match.arg(class, choices = taxon_classes(x), several.ok = TRUE)
  
  # return
  return(class)
}