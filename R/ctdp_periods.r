#' @title Slice deployments with periods
#' @description Slice deployments with periods
#' @param x an object of class \code{\link{ctdp}}
#' @param y a \code{tibble} with columns "name" and "interval": (e.g. the result of function \code{\link{set_periods}})
#' @return an object of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso \code{\link{set_periods}}
#' @examples \dontrun{
#' # get overall time range of data
#' time_range(camsample)
#' plot_time_coverage(camsample)
#' 
#' # Set period intervals
#' periods <- set_periods(start = c("4-21 08:00", "6-21 08:00"),
#'                        end   = c("5-19 18:00", "7-19 18:00"), 
#'                        orders = "md HM",
#'                        years = 2017,
#'                        tz = "Etc/GMT+5")
#' 
#' # Slice the ctdp object
#' y <- ctdp_periods(camsample, periods)
#' y
#' y$deployments
#' y$deployments %>% count(periodName)
#' plot_time_coverage(y)
#' 
#' # Process further
#' y <- filter_deployments(y, !is.na(periodName))
#' y
#' plot_time_coverage(y)
#' }
#' @export
ctdp_periods <- function(x, y) {
  # Checks
  warning("still a rudimentary function; need to rewrite, implement checks, etc")
  
  # Inputs, checks
  # x
  # y
  
  # "period" column already exists?
  if("period" %in% names(x$deployments)){
    stop("period column already exists in $deployments, probably ctdp_periods has already been called on the object x?")
  }
  
  # sort y
  y <- y %>% 
    arrange(int_start(interval))
  
  # Truncate all to intersect only timerange of x
  xint <- int_diff(time_range(x))
  y <- y %>% 
    mutate(interval = lubridate::intersect(interval, xint))
  
  # remove intervals that are of length 0
  y <- y %>% 
    filter(int_length(interval) > 0)
  
  # Prep y
  {
    periods_all <- y %>% 
      mutate(start = int_start(interval),
             end = int_end(interval)) %>% 
      select(-interval) %>% 
      pivot_longer(c(start, end), names_to = "poit", values_to = "time") %>% 
      bind_rows(tibble(name = "ctdp",
                       poit = c("start", "end"),
                       time = time_range(x))) %>% 
      arrange(time) %>% 
      mutate(name_poit = str_c(name, poit, sep = "_"))
    # periods_all
    
    # Get all intervals  
    periods_int <- periods_all %>% 
      pull(time) %>% 
      unique() %>% 
      int_diff()
    # periods_int
    
    # Data table with START of each interval, for rolling join
    periods_start <- tibble(pid = seq_along(periods_int),
                            int = periods_int,
                            time = int_start(int)) %>% 
      mutate(pstart = time,
             pend   = int_end(int),
             iny = int %within% as.list(y$interval))
    # periods_start
    
    periods_start_dt <- periods_start %>% 
      select(pid, time, pstart, pend) %>% 
      data.table(key = "time")
    # periods_start_dt
    }
  # periods_all
  # periods_int
  # periods_start
  # periods_start_dt
  
  # Prep $sequences
  xseq_dt <- x$sequences %>% 
    mutate(row = row_number()) %>% 
    select(row, sequenceID, deploymentID, sequence_interval) %>% 
    mutate(time = int_start(sequence_interval)) %>% 
    select(-sequence_interval) %>% 
    data.table(key = "time")
  # xseq_dt
  
  # Forward rolling join to $sequences
  xseq_dt_roll <- periods_start_dt[xseq_dt, roll=TRUE] %>% 
    as_tibble() %>% 
    arrange(row)
  # xseq_dt_roll
  # xseq_dt_roll %>% 
  # count(pid) # LAST one is only the time point of closing
  
  # update x$sequences$deploymentID (append with _periodX where X is nr [pid])
  x$sequences <- x$sequences %>% 
    mutate(period = xseq_dt_roll$pid, 
           deploymentID = str_c(deploymentID, period, sep = "_period")) %>% 
    select(-period)
  
  # Do the same with deployments:
  {
    xdep <- x$deployments %>%
      mutate(row = row_number()) %>% 
      select(row, deploymentID, deployment_interval)
    xdepReplicated <- list()
    for(i in seq_len(nrow(xdep))){
      xdepi <- xdep[i,]
      xdepi_rep <- int_overlaps(xdepi$deployment_interval, periods_start$int)
      xdepi_rep <- which(xdepi_rep)
      xdepi <- xdepi[rep(1, length(xdepi_rep)),]
      xdepi$pid <- xdepi_rep
      xdepReplicated[[i]] <- xdepi
    }
    xdepReplicated <- bind_rows(xdepReplicated)
    # xdepReplicated
    
    xdepReplicated <- xdepReplicated %>% 
      mutate(poi_int = periods_start$int[pid],
             PINT = lubridate::intersect(deployment_interval, poi_int))
    # xdepReplicated
    
    xdepReplicated <- xdepReplicated %>% 
      select(row, deploymentID, pid, PINT) %>% 
      rename(deployment_interval = PINT,
             period = pid) %>% 
      mutate(deploymentIDnew = str_c(deploymentID, period, sep = "_period"))
    # xdepReplicated
    
    # xdepReplicated %>% count(row) %>% count(n)
    # xdepReplicated %>% count(deploymentID) %>% filter(n>4)
    
    # xdepReplicated %>% filter(deploymentID == "013fe229-de87-4e28-b7f5-4bb33983551b")
  }
  # xdepReplicated
  
  # Remove intervals where the length is 0 (e.g. the final image in a deployment)
  {
    xdepReplicated <- xdepReplicated %>% 
      filter(int_length(deployment_interval) > 0)
    
    # CHECKS
    # x$sequences
    # x$sequences %>% anti_join(xdepReplicated, by = c("deploymentID" = "deploymentIDnew"))
    # xdepReplicated %>% anti_join(x$sequences, by = c("deploymentIDnew" = "deploymentID"))
  }
  
  
  ### Put in x
  xdep_org <- x$deployments %>%
    mutate(row = row_number())   
  xdep_new <- xdepReplicated %>% 
    select(row, period, deploymentIDnew, deployment_interval) %>% 
    rename(deploymentID = deploymentIDnew)
  x$deployments <- xdep_org %>% 
    select(-deploymentID, -deployment_interval) %>% 
    left_join(xdep_new, by = "row", multiple = "all") %>% 
    select(all_of(names(xdep_org)),
           period) %>% 
    select(-row) %>% 
    relocate(period, .after = deploymentID)
  
  
  ### Retrieve period name from y
  # periods_start
  # y
  period_names <- periods_start %>% 
    mutate(periodNAME = NA_character_)
  
  if(identical(y$interval, filter(period_names, iny == TRUE)$int)){
    period_names$periodNAME[period_names$iny] <- y$name
    period_names <- period_names %>% 
      select(pid, periodNAME) %>% 
      rename(periodName = periodNAME)
    x$deployments <- x$deployments %>% 
      left_join(period_names, by = c("period" = "pid")) %>% 
      relocate(periodName, .after = period)
  }else{
    stop("period names do not match length")
  }
  
  # return updated version of x
  return(x)
}