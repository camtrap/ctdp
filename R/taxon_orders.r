#' Retrieve the taxonomic orders
#' @param x an object of class \code{\link{ctdp}}
#' @inheritParams class_void
#' @return a \code{character} vector with the taxonomic orders present in \code{x}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' taxon_orders(camsample)
#' taxon_orders(camsample, class = taxon_classes(camsample))
#' taxon_orders(camsample, class = "Mammalia")
#' }
#' @export
taxon_orders <- function(x, class = NULL) {
  # Get taxonomy table
  y <- x$taxonomy
  
  if(!is.null(class)){
    doClass <- class_argcheck(x, class)
    y <- y %>% 
      filter(class %in% doClass)
  }
  
  y <- y %>% 
    count(order) %>% 
    ungroup() %>% 
    drop_na() %>% 
    pull(order)
  return(y)
}