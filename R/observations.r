#' Retrieve the observations table of a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @return a \code{tibble} with the observations table
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' observations(camsample)
#' }
#' @export
observations <- function(x) {
  y <- x$observations
  return(y)
}