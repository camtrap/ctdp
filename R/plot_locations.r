#' Plot locations
#' @param x an object of class \code{\link{ctdp}}
#' @param doPlot \code{logical} whether or not to plot the leaflet map, defaults to \code{TRUE}
#' @return silently returns the leaflet plot
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' plot_locations(camsample)
#' }
#' @export
plot_locations <- function(x, doPlot = TRUE) {
  # get locations
  z <- x$locations
  
  # Add popup column
  z <- z %>% 
    mutate(popup = str_c("ID: ",locationID,
                         "<br>Name: ",locationName))
  
  # Prepare
  symbols <- makeSymbolsSize(
    values = 3,
    shape = 'circle',
    color = "black",
    fillColor = "grey",
    opacity = .75,
    baseSize = 10
  )
  
  # Plot
  p <- z %>% 
    leaflet() %>% 
    addTiles() %>%
    addMarkers(data = z,
               icon = symbols,
               lng = ~longitude,
               lat = ~latitude,
               popup = ~popup)
  if(doPlot){
    print(p)
  }
  
  invisible(p)
}