#' @title Remove deployments with insufficient (meta)data
#' @description Remove deployments with insufficient (meta)data
#' @details This function removes deployments when there either is no depoloyment identifier, or there is an identifyer but no correct deployment interval (NA for start and/or end) _and_ not sufficient sequence data (\code{< minNrSeqs} records)
#' @param x a \code{ctdp} object
#' @param minNrSeqs minimum \code{integer} number of sequences (deployment is removed if there are \code{< minNrSeqs} records)
#' @return a \code{ctdp} object
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @export
rm_empty_deployments <- function(x, minNrSeqs = 2) {
  # input checks
  if(is_ctdp(x) == FALSE) { stop("x should be of class ctdp")}

  # Remove records in the $deployment table if there is NA in deploymentID
  x$deployments <- x$deployments %>% 
    drop_na(deploymentID)
  
  # IDEM locations
  x$locations <- x$locations %>% 
    drop_na(locationID)
  
  # Check whether there are NAs in any of the deployment intervals
  if(is.na(as.numeric(diff(time_range(x, based_on = "deployments"))))){
    # At least 1 NA somewhere
    dep_length <- map_dbl(x$deployments$deployment_interval, int_length)
    
    # Which deployment NA?
    dep_na <- which(is.na(dep_length))
    dep_naID <- x$deployments$deploymentID[dep_na]
    
    # list of deploymentIDs to remove
    rmDepIDs <- list()
    ii <- 1
    
    # Infer from sequences
    for(i in seq_along(dep_na)){
      # Get focal deployment
      iID <- dep_naID[i]
      
      # filter sequence info
      iSeq <- x$sequences %>% 
        select(deploymentID, sequence_interval) %>% 
        filter(deploymentID == iID)
      
      # IS there sequence data for this depoloyment?
      if(nrow(iSeq) < minNrSeqs) {
        rmDepIDs[[ii]] <- iID
        ii <- ii + 1L
      }
    }
    
    # Process if there are deployments to remove
    if(length(rmDepIDs) > 0) {
      # unlist
      rmDepIDs <- unlist(rmDepIDs)
      
      # Remove 
      x$deployments <- x$deployments %>% 
        filter(! deploymentID %in% rmDepIDs)
      x$sequences <- x$sequences %>% 
        filter(! deploymentID %in% rmDepIDs)
      
      # Also rest
      keepSeqIDs <- x$sequences$sequenceID
      x$observations <- x$observations %>% 
        filter(sequenceID %in% keepSeqIDs)
      x$media <- x$media %>% 
        filter(sequenceID %in% keepSeqIDs)
    }
  }

  # Return
  return(x)
}