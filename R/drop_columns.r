#' drop columns from a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @return x an object of class \code{\link{ctdp}} with columns removed (except for important columns)
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}, \code{\link{ctdp_mainCols}}
#' @examples \dontrun{
#' drop_columns(camsample)
#' }
#' @export
drop_columns <- function(x){
  # Get tibble with list-column per table to keep
  keepCols <- ctdp_mainCols %>% 
    group_by(table) %>% 
    nest() %>% 
    ungroup()
  
  # copy x to y
  y <- x

  # remove columns
  for(i in keepCols$table){
    pullCols <- keepCols %>% 
      filter(table == i) %>% 
      pull(data) %>% 
      .[[1]] %>% 
      pull(column)
    
    y[[i]] <- y[[i]] %>% 
      select(any_of(pullCols))
  }
  
  # return y
  return(y)
}