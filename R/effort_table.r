#' Get a step function with the number of active cameras over time
#' @param x an object of class \code{\link{ctdp}}
#' @param startend \code{logical}, defaults to FALSE, whether or not to include the endpoints of the stepfunction, or just the switches to new values.
#' @return a \code{tibble} with effort data
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' effort_table(camsample)
#' effort_table(camsample, startend = TRUE)
#' }
#' @keywords internal
#' @export
effort_table <- function(x, startend = FALSE) {
  x <- ctdp_interval(x)
  x_start <- int_start(x$deployments$deployment_interval)
  x_end <- int_end(x$deployments$deployment_interval)
  if(length(start) != length(end)) { stop("start and end have different lengths") }
  n <- length(start)
  dt <- as.numeric(max(x_end)) - as.numeric(min(x_start))
  effort <- bind_rows(
    tibble(time = min(x_start) - 0.025 * dt,
           add = 0L),
    tibble(time = x_start,
           add = +1L),
    tibble(time = x_end,
           add = -1L),
    tibble(time = max(x_end) + 0.025 * dt,
           add = 0L))
  effort <- effort %>% 
    group_by(time) %>% 
    summarize(add = sum(add)) %>% 
    ungroup()
  effort <- effort %>% 
    arrange(time) %>% 
    mutate(nrCams = cumsum(add))
  # plot(effort$time, effort$nrCams, type="s")
  effort <- effort %>% 
    select(-add)
  
  # only start of new value, or start and end of value?
  if(startend){
    n <- nrow(effort)
    effort <- tibble(time = c(effort$time[1],
                              rep(effort$time[-1], each = 2),
                              effort$time[n]),
                     nrCams = rep(effort$nrCams, each=2))
  }
  
  # Return
  return(effort)
}