#' Retrieve the deployments table of a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @return a \code{tibble} with the deployments table
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' deployments(camsample)
#' }
#' @export
deployments <- function(x) {
  y <- x$deployments
  return(y)
}