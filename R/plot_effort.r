#' Plot a step function with the number of active cameras over time
#' @param x an object of class \code{\link{ctdp}}
#' @param dynamic \code{logical} whether or not to plot a dynamic graph (via \code{\link{dygraph}}, defaults to \code{TRUE}
#' @param main \code{character} plot title, defaults to "Effort"
#' @param xlab \code{character} x axis label, defaults to "time"
#' @param ylab \code{character} y axis label, defaults to "nr of active cams"
#' @param ... other arguments passed on to the plot function
#' @return a plot (whenb \code{doPlot=TRUE}), and (silently when plotting) it returns a tibble with effort data
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' plot_effort(camsample)
#' }
#' @export
plot_effort <- function(x, dynamic = TRUE, main = "Effort", xlab = "time", ylab = "nr of active cams", ...) {
  z <- effort_table(x, startend = TRUE)
  if(dynamic){
    series <- xts(z$nrCams, order.by = z$time, tz="GMT")
    dygraph(series, main=main, xlab=xlab, ylab=ylab, ...) %>%
      dyOptions(fillGraph = TRUE, fillAlpha = 0.4) %>% 
      dyRangeSelector()
  }else
  {
    plot(z$time, z$nrCams, type="n", main=main, xlab=xlab, ylab=ylab, ...)
    xx <- c(z$time, rev(z$time))
    yy <- c(z$nrCams, rep(0, length(z$nrCams)))
    polygon(xx, yy, border = NA, col=8)
    lines(z$time, z$nrCams, type="s")
  }
}