#' Retrieve the time zone of a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @return a \code{character} string with the time zone information of the date-time objects in \code{x}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' time_zone(camsample)
#' }
#' @export
time_zone <- function(x) {
  x$settings$tz
}