#' @title Retrieve the locations of a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @return a \code{tibble} with the camera trap locations
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' locations(camsample)
#' locations(camsample, addLonlat = FALSE)
#' }
#' @export
locations <- function(x, addLonlat = TRUE) {
  y <- x$locations
  if(addLonlat == FALSE)
  {
    ynames = names(y)
    ynames = ynames[! tolower(ynames) %in% c("longitude","latitude","lon","lat")]
    y <- y %>%
      select(all_of(ynames))
  }
  return(y)
}