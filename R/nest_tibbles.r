#' Join the ctdp tibbles into 1 tibble with 1 row per sequence
#' @param x an object of class \code{\link{ctdp}}
#' @param dropMedia \code{logical}, indicating whether or not to remove the \code{media} information. Defaults to \code{FALSE}
#' @param onlyDistinct \code{logical}, indicating whether or not to remove the duplicate rows. Defaults to \code{TRUE}
#' @param mainCols \code{character} vector with the names of those columns that should be kept outside a list-column (not from media). Defaults to \code{c("captureMethod","nrphotos")}.
#' @return object of class \code{seqnest}, which is a \code{tibble} with columns \code{sequenceID}, all columns in \code{mainCols}, and \code{list-clumns} "locations", "deployments", "observations" and "media". All these \code{list-columns} contain data in \code{data.table} format.
#' @author Henjo de Knegt
#' @seealso \code{\link{merge_tibbles}}
#' @examples \dontrun{
#' nest_tibbles(camsample)
#' }
#' @export
nest_tibbles <- function(x,
                         dropMedia = FALSE,
                         onlyDistinct = TRUE,
                         mainCols = c("captureMethod","nrphotos")) {
  ### Convert interval to _start/_end
  x <- ctdp_interval(x)
  x <- ctdp_interval(x, rev = TRUE)
  
  
  # has media?
  if(x$settings$media == FALSE) {
    dropMedia <- TRUE
  }
  
  # Update mainCols
  intervalNames <- dttmColNames(x)
  mainCols <- unique(c(as.character(unlist(intervalNames$sequences)), mainCols))

  
  ### get keys and
  keys <- keycolNames()
  linkStructure <- linkStructure()


  ### First: convert to 1 big tibble
  yall <- merge_tibbles(x, dropMedia = dropMedia, onlyDistinct = onlyDistinct, toInterval = FALSE)
  # yall
  ydrop <- yall

  
  ### get MEDIA
  if(dropMedia == FALSE) {
    # Get media data
    omitKeys <- keys[-which(names(keys) %in% linkStructure$media)] %>% unlist() %>% as.character()
    lc_media <- x$media %>%
      select(-any_of(omitKeys)) %>%
      select("sequenceID", everything()) %>%
      distinct() %>%
      data.table(key = "sequenceID")
    lc_media <- lc_media %>%
      dt_nest(sequenceID, .key="media")
  }
  # lc_media
  # lc_media$media[[1]]
  
  
  ### get MAIN
  {
    # CHECK uniqueness
    lc_main <- yall %>%
      select(all_of(c("sequenceID", mainCols))) %>%
      distinct()
    if(length(lc_main$sequenceID) != length(unique(lc_main$sequenceID))){
      stop("non unique main columns found per sequence")
    }
    lc_main <- lc_main %>%
      data.table(key = "sequenceID") %>%
      dt_nest(sequenceID, .key="main")
    ydrop <- ydrop %>%
      select(-all_of(mainCols))
  }
  # lc_main
  # lc_main$main[[1]]

  
  ### get OBSERVATIONS
  {
    useNames <- names(x$locations)
    useNames <- useNames[! useNames %in% as.character(unlist(keys))]
    useNames <- useNames[! useNames %in% mainCols]
    lc_locations <- ydrop %>%
      select(sequenceID, locationID, all_of(useNames)) %>%
      distinct() %>%
      data.table(key = "sequenceID") %>%
      dt_nest(sequenceID, .key="locations")
    ydrop <- ydrop %>%
      select(-all_of(names(lc_locations$locations[[1]])))
  }
  # lc_locations
  # lc_locations$locations[[1]]
  
  
  ### get DEPLOYMENTS
  {
    useNames <- names(x$deployments)
    useNames <- useNames[! useNames %in% as.character(unlist(keys))]
    useNames <- useNames[! useNames %in% mainCols]
    lc_deployments <- ydrop %>%
      select(sequenceID, deploymentID, all_of(useNames)) %>%
      distinct() %>%
      data.table(key = "sequenceID") %>%
      dt_nest(sequenceID, .key="deployments")
    ydrop <- ydrop %>%
      select(-all_of(names(lc_deployments$deployments[[1]])))
  }
  # lc_deployments
  # lc_deployments$deployments[[1]]

  
  ### get OBSERVATIONS/TAXONOMY
  {
    useNames <- c(names(x$observations), names(x$taxonomy))
    useNames <- useNames[! useNames %in% as.character(unlist(keys))]
    useNames <- useNames[! useNames %in% mainCols]
    lc_observations <- ydrop %>%
      select(sequenceID, observationID, taxonID, all_of(useNames)) %>%
      distinct() %>%
      data.table(key = "sequenceID") %>%
      dt_nest(sequenceID, .key="observations")
    ydrop <- ydrop %>%
      select(-all_of(names(lc_observations$observations[[1]])))
  }
  # lc_observations
  # lc_observations$observations[[1]]

  
  ### what is left
  # ydrop
  
  
  ### COMBINE
  {
    # Checks
    if(length(unique(unlist(lapply(list(lc_main, lc_locations, lc_deployments, lc_observations), nrow)))) != 1){stop("not all list-column tibbles have the same nrow")}
    if(all(unlist(lapply(list(lc_main, lc_locations, lc_deployments, lc_observations), key)) == "sequenceID") == FALSE){stop("not all list-column tibbles have key sequenceID")}
    
    # Unnest main
    lc_ALL <- lc_main %>%
      dt_unnest(main)
    # key(lc_ALL)
    lc_ALL <- lc_ALL[lc_locations][lc_deployments][lc_observations]
    if(dropMedia == FALSE) {
      lc_mediaSUB <- lc_media[lc_main[,"sequenceID",with=FALSE]]
      lc_ALL <- lc_ALL[lc_mediaSUB]
    }
    # lc_ALL
    lc_ALL <- as_tibble(lc_ALL)
  }
  # lc_ALL

  
  ### Sequences start/end to interval
  lc_ALL <- lc_ALL %>%
    mutate(sequence_interval = interval(start = sequence_start, end = sequence_end)) %>%
    relocate(sequence_interval, .before = sequence_start) %>%
    select(-sequence_start, -sequence_end)

  
  ### Assign class seqnest
  class(lc_ALL) <- c("seqnest", class(lc_ALL))
  
  
  ### Check nrow of locations and deployments: should be 1!
  lc_ALL_check <- lc_ALL %>%
    mutate(nr_locs = map_int(locations, nrow),
           nr_deps = map_int(deployments, nrow))
  if(all(lc_ALL_check$nr_locs == 1L) == FALSE){stop("locations list column has elements with > 1 records")}
  if(all(lc_ALL_check$nr_deps == 1L) == FALSE){stop("deployments list column has elements with > 1 records")}
  

  ### Return
  return(lc_ALL)
}

#' @export
print.seqnest <- function(x)
{
  cat("Object of class 'seqnest': a 'tibble' where each row represents a single sequence (with identifier: sequenceID), and corresponding data in list-columns.\n\n")
  class(x) <- class(x)[-1]
  print(x)
}