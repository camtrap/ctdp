#' Retrieve the primary keys of the ctdp tables
#' @param x an object of class \code{\link{ctdp}}
#' @return a named \code{list} with \code{character} names of the primary keys of the tibbles in \code{x}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' primary_keys(camsample)
#' }
#' @export
primary_keys <- function(x) {
  keycolNames()
}