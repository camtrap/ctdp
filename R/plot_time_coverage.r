#' Plot activity of the camera-stations over time
#' @param x an object of class \code{\link{ctdp}}
#' @param IDcol \code{character} name of the column that groups the deployments, defaults to "locationName
#' @param xlab,ylab \code{character} names of the axis labels (defaults to "Time" and "Location")
#' @param grouping an optional \code{character} vector with the name(s) of the grouping columns, defaults to \code{NULL}
#' @param colour an optional \code{character} name of the column in the deployments table to be used for colouring the polygons (this overwrites the \code{grouping} argument, which will then only be used for the y-axis ordering). Defaults to \code{NULL}
#' @param dynamic \code{logical} whether or not to plot a dynamic graph (via \code{ggplotly}, defaults to \code{TRUE}
#' @param addSequences \code{logical} whether or not to add the times of sequences to the plots (with jittering in y-direction), defaults to \code{FALSE}
#' @param jitter \code{numeric} value of the amount of vertical jittering (sd of normal distribution centered on 0), defaults to 0.05.
#' @param size \code{numeric} value of the size of the points, defaults to 0.25
#' @param doTimelapse \code{logical} whether or not to plot the time lapse sequences, defaults to \code{FALSE}
#' @param doPlot \code{logical} whether or not to plot, defaults to \code{TRUE}
#' @return invisibly returns the plot
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' # dynamic ggplotly plots
#' plot_time_coverage(camsample)
#' plot_time_coverage(camsample, addSequences = TRUE)
#' 
#' # only ggplot
#' plot_time_coverage(camsample, dynamic = FALSE)
#' plot_time_coverage(camsample, addSequences = TRUE, dynamic = FALSE)
#' 
#' # colour by grouping
#' plot_time_coverage(camsample, grouping = "transect")
#' }
#' @export
plot_time_coverage <- function(x,
                               IDcol = "locationName", 
                               xlab = "Time", ylab = "Location", 
                               grouping = NULL,
                               colour = NULL,
                               dynamic = TRUE,
                               
                               addSequences = FALSE,
                               jitter = 0.05,
                               size = 0.25,
                               doTimelapse = FALSE,
                               
                               doPlot = TRUE) {
  # Force to interval
  x <- ctdp_interval(x)
  
  # Prep data for deployments
  dfCols <- unique(c("deploymentID","locationName","deployment_interval",IDcol,grouping,colour))
  df <- x$deployments %>% 
    left_join(x$locations, by = "locationID") %>% 
    select(all_of(dfCols)) %>% 
    rename(id = deploymentID,
           int = deployment_interval)
  # df
  df[,"label"] <- df[,IDcol]
  df[,IDcol] <- NULL
  df$label = as.character(df$label)
  
  # set colour
  if(!is.null(colour)){
    df[,"colour"] <- df[,colour]
    if(colour != "colour") { df[,colour] <- NULL }
  }else{
    df[,"colour"] <- 1
  }
    
  # set grouping
  if(!is.null(grouping)){
    df[,"group"] <- df[,grouping]
    if(grouping != "group") { df[,grouping] <- NULL }
    df$group <- as.factor(df$group)
    df$z = interaction(df$group, df$label)
  }else{
    df$group <- as.factor(df$label)
    df$z = as.factor(df$label) 
  }
  df$z <- droplevels(df$z)
  df$znr = as.numeric(as.integer(df$z))
  df <- df %>% 
    arrange(znr)
  # plot(df$znr)

  
  ### Summarise to prepare for plotting
  dfsum <- df %>% 
    group_by(z, znr, id, label, group, colour) %>% 
    summarise(xmin = int_start(int),
              xmax = int_end(int),
              ymin = znr - 0.4,
              ymax = znr + 0.4,
              .groups = "drop")
  # dfsum

  # Add identifier
  dfsum <- dfsum %>% 
    mutate(location = paste(label, id, sep = ": deployment: "))

    
  ### set up base ggplot
  p <- ggplot()
  # p
  
  
  ### Add deployment rectangles
  if(!is.null(grouping)){
    if(is.null(colour)){
      p <- p +
        geom_rect(mapping = aes(xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax,
                                group = location,
                                fill = group),
                  data = dfsum)
    }else{
      p <- p +
        geom_rect(mapping = aes(xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax,
                                group = location,
                                fill = colour),
                  data = dfsum)
    }
  }else{
    if(is.null(colour)){
      p <- p +
        geom_rect(mapping = aes(xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax,
                                group = location),
                  data = dfsum)
    }else{
      p <- p +
        geom_rect(mapping = aes(xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax,
                                group = location,
                                fill = colour),
                  data = dfsum)
    }
  }
  # p
  
  # Add colour scale if colour = "fracAnnotated"
  if(!is.null(colour)){
    if(colour == "fracAnnotated"){
      mycols <- c("navy", "blue", "cyan", "lightcyan", "yellow", "red", "red4")
      p <- p +
        scale_fill_gradientn(colours = mycols, limits = c(0,1)) + 
        labs(fill = "Annotated")
    }
  }else{
    if(!is.null(grouping)){
      p <- p +
        labs(fill = str_to_sentence(grouping))
    }
  }
 
  ### Add points from sequences?
  if(addSequences){
    dfSeq <- x$sequences %>% 
      left_join(x$deployments, by = "deploymentID") %>% 
      left_join(x$locations, by = "locationID") %>% 
      select(all_of(c("sequence_interval", "captureMethod", dfCols))) %>% 
      select(-deployment_interval) %>% 
      rename(id = deploymentID,
             int = sequence_interval)
    
    # Filter out the time lapse sequences
    if(doTimelapse == FALSE){
      dfSeq <- dfSeq %>% 
        filter(captureMethod != "time lapse")
    }
    
    # dfSeq
    dfSeq[,"label"] <- dfSeq[,IDcol]
    dfSeq[,IDcol] <- NULL
    dfSeq$label = as.character(dfSeq$label)
    
    # Get grouping
    if(!is.null(grouping)){
      dfSeq[,"group"] <- dfSeq[,grouping]
      if(grouping != "group") { dfSeq[,grouping] <- NULL }
      dfSeq$z = interaction(df$group, dfSeq$label)
    }else{
      dfSeq$group <- as.character(dfSeq$label)
      dfSeq$z = as.character(dfSeq$label)
    }
    dfSeq$z = factor(dfSeq$z, levels = levels(df$z)) 
    dfSeq$group <- factor(as.character(dfSeq$group), levels = levels(df$group))
    dfSeq$znr = as.numeric(as.integer(dfSeq$z))
    dfSeq <- dfSeq %>% 
      arrange(znr)
    
    # add jitter and subset cols
    dfSeq <- dfSeq %>% 
      mutate(location = paste(label, id, sep = ": deployment: ")) %>% 
      mutate(seq_start = int_start(int),
             znr_jitter = znr + rnorm(n = n(), mean = 0, sd = jitter)) %>% 
      select(id, seq_start, znr_jitter)
    # dfSeq %>% filter(is.na(seq_start))
    # dfSeq %>% filter(is.na(znr_jitter))
    # summary(dfSeq$znr_jitter)
    
    # add points
    p <- p + 
      geom_point(data = dfSeq,
                 mapping = aes(x = seq_start,
                               y = znr_jitter),
                 col = "red", size = size)
  }else{
    p <- p
  }
  # p
  

  ## add labels etc
  p <- p +
    xlab(xlab) +
    ylab(ylab) + 
    theme_linedraw()
  
  # update y-axis (x because of coord flip) to labels
  yAxVals <- dfsum %>% 
    select(znr,label) %>% 
    distinct() %>% 
    arrange(znr)
  p2 <- p + 
    scale_y_discrete(limits = as.character(yAxVals$znr),
                     labels = yAxVals$label)
  # p2
  

  ### dynamic ggplotly plot or just ggplot?
  if(dynamic) {
    p2 <- ggplotly(p2, tooltip = "location")
  }
  
  
  ### Plot?
  if(doPlot){
    print(p2)
  }

  
  ### Invisibly return
  invisible(p2)
}