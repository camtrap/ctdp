#' @title Specify a timezone via an offset relative to GMT/UTC
#' @description Specify a timezone via an offset relative to GMT/UTC
#' @param offset a (signed) \code{integer} or \code{numeric} (that can be parsed to integer) value with the offset relative to GMT/UTC (in hours)
#' @author Henjo de Knegt
#' @seealso NULL
#' @return a \code{character} notation of the timezone
#' @examples \dontrun{
#' set_GMT_offset(+2)
#' set_GMT_offset(-5)
#' }
#' @export
set_GMT_offset <- function(offset) {
  # Checks
  if(is.numeric(offset) == FALSE){stop("offset must be numeric/integer")}
  if(is.integer(offset) == FALSE){
    if(offset %% 1 == 0){
      offset <- as.integer(offset)
    }else{
      stop("if offset is supplied as numeric value, it should be a whole number")
    }
  }
  if(offset < -12 | offset > +12){stop("offset should be in range -12 -- +12")}
  if(offset >= 0){
    tz <- str_c("Etc/GMT-",abs(offset))
  }else{
    tz <- str_c("Etc/GMT+",abs(offset))
  }
  return(tz)
}