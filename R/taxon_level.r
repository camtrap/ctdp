#' @title Get taxonomic class of a species by its identifier
#' @description Get taxonomic class of a species by its identifier
#' @param taxonID a \code{character} string with taxonID
#' @param level a \code{character} string with the desired level, with choices "species","genus","family","suborder","order","infraclass","subclass","class","superclass","megaclass","gigaclass","parvphylum","infraphylum","subphylum","phylum","kingdom","unranked"
#' @author Henjo de Knegt
#' @seealso API of \url{https://www.catalogueoflife.org/} called
#' @return a \code{character} string with class of the taxon
#' @examples \dontrun{
#'   taxon_level("QLXL", level = "species")
#'   taxon_level("QLXL", level = "genus")
#'   taxon_level("QLXL", level = "family")
#'   taxon_level("QLXL", level = "suborder")
#'   taxon_level("QLXL", level = "order")
#'   taxon_level("QLXL", level = "infraclass")
#'   taxon_level("QLXL", level = "subclass")
#'   taxon_level("QLXL", level = "class")
#'   taxon_level("QLXL", level = "superclass")
#'   taxon_level("QLXL", level = "megaclass")
#'   taxon_level("QLXL", level = "gigaclass")
#'   taxon_level("QLXL", level = "parvphylum")
#'   taxon_level("QLXL", level = "infraphylum")
#'   taxon_level("QLXL", level = "subphylum")
#'   taxon_level("QLXL", level = "phylum")
#'   taxon_level("QLXL", level = "kingdom")
#'   taxon_level("QLXL", level = "unranked")
#' }
#' @export
taxon_level <- function(taxonID, level = "class") {
  # Checks on inputs
  if(missing(taxonID)){stop("supply input taxonID")}
  level <- match.arg(level, choices = c("species","genus","family","suborder","order","infraclass","subclass","class",
                                        "superclass","megaclass","gigaclass","parvphylum","infraphylum","subphylum",
                                        "phylum","kingdom","unranked"))
  
  # Get json string from api and parse to list
  y <- fromJSON(curl(paste0("https://api.catalogueoflife.org/dataset/9854/tree/",taxonID)))
  
  # Get taxonomic level (if not empty list!)
  if(is.data.frame(y)) {
    y <- subset(y, rank == level)$name 
    if(length(y) == 0) {
      y <- NA_character_
    }
  }else{
    y <- NA_character_
  }
  
  # return
  return(y)
}