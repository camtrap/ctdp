#' @title Create a tutorial in html format
#' @description Create a tutorial in html format
#' @details NULL
#' @param x an object of class \code{ctdp}
#' @param fileName an optional filename for the resultant html file, defaults to \code{NULL}
#' @param title an optional title for the tutorial, defaults to "Package ctdp - tutorial"
#' @param focalSpecies \code{character} name of the species to focus on, see \code{\link{taxon_id}}. Defaults to NULL, in which case the tutorial will focus on the 3 most abundant mammalian species.
#' @return a .html tutorial is being saved to the working directory, with the filename of ctdp zip that was loaded (with suffix "_tutorial.html").
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples NULL
#' @export
write_tutorial <- function(x,
                           fileName = NULL,
                           title = "Package ctdp - tutorial",
                           overwrite = TRUE,
                           focalSpecies = NULL,
                           start = NULL,
                           end = NULL) {
  # Get project properties
  fileNameInfo <- pathProperties(x)
  zipName <- str_remove(fileNameInfo$fileFolder, pattern = ".zip")
  
  # Get folder/file to write tutorial to
  if(is.null(fileName)){
    saveFileName <- paste0(zipName,"_tutorial.html")
    saveFolder <- getwd()
  }else{
    fileNameInfo <- pathProperties(fileName)
    saveFileName <- str_remove(fileNameInfo$fileFolder, pattern = ".html")
    saveFileName <- paste0(saveFileName,".html")
    saveFolder <- fileNameInfo$path
  }
  # saveFileName
  # saveFolder
  
  # Get focalSpecies as 3 most frequently observed mammalian species (if NULL)
  if(is.null(focalSpecies)){
    # Count most frequent 3 mammalian species
    focalSpecies <- x$observations %>% 
      left_join(x$taxonomy, by = "taxonID") %>% 
      select(observationType, count, class, order, vernacularNames.eng) %>% 
      filter(observationType == "animal") %>% 
      filter(class == "Mammalia") %>% 
      drop_na() %>% 
      group_by(vernacularNames.eng) %>% 
      summarise(count = sum(count)) %>% 
      arrange(desc(count)) %>% 
      slice_head(n = 3L) %>% 
      pull(vernacularNames.eng)
  }
  
  # create temp folder and set wd in it
  tmpDir <- tempdir(check = TRUE)
  if(dir.exists(tmpDir) == FALSE) { stop("temporary directory not found") }
  orgWD <- getwd()
  setwd(tmpDir)
  cat("Used temporary directory: ", gsub('"', "", gsub("\\\\", "/", tmpDir)), "\n")
  
  # Get parms list
  parms <- list(path = NA,
                data = x,
                title = title,
                focalSpecies = focalSpecies,
                start = ifelse(!is.null(start), start, NA),
                end = ifelse(!is.null(end), end, NA))
  
  # Copy Rmd file to tmpDir
  file.copy(from = system.file("extdata", "ctdp_tutorial.Rmd", package = "ctdp"),
            to = tmpDir,
            overwrite = TRUE)
  
  # Local functions to catch result from rendering
  .f_call <- function(f, ...) {
    fArgList = list(...)
    y = do.call(f, args=fArgList)
    return(y)
  }
  .f_try = function(f, ...) {
    # https://stackoverflow.com/questions/4948361/how-do-i-save-warnings-and-errors-as-output-from-a-function
    warn <- err <- NULL
    value <- withCallingHandlers(
      tryCatch(.f_call(f, ...), error=function(e) {
        err <<- e
        NULL
      }), warning=function(w) {
        warn <<- w
        invokeRestart("muffleWarning")
      })
    
    # Return named list with value, warning and error
    list(value=value,
         warning=warn,
         error=err)
  }
  .f_render <- function(p){
    rmarkdown::render(input = "ctdp_tutorial.Rmd",
                      output_format  = "html_document",
                      output_file = "ctdp_tutorial.html",
                      params = p,
                      clean = FALSE)
    return(TRUE)
  }
  
  # Render tutorial (in trycatch, in order to catch any errors or warnings)
  result <- FALSE
  result <- .f_try(.f_render, p = parms)

  # set back WD
  setwd(orgWD)
  
  # Copy output html back to wd with new filename
  if(is.null(result$error)){
    # Copy
    file.copy(from = file.path(tmpDir, "ctdp_tutorial.html"),
              to = file.path(saveFolder, saveFileName),
              overwrite = overwrite)

    # Cat 
    cat("Tutorial written to:",file.path(saveFolder, saveFileName),"\n")
    
    # Remove ctdp_tutorial.Rmd in tmp dir
    file.remove(file.path(tmpDir, "ctdp_tutorial.Rmd"))
    file.remove(file.path(tmpDir, "ctdp_tutorial.html"))
    file.remove(file.path(tmpDir, "ctdp_tutorial.knit.md"))
  }else{
    stop(paste("Error occurred, see dir:",tmpDir))
  }
}