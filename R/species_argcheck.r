#' @title Argument check on species
#' @inheritParams ctdp
#' @inheritParams species_void
#' @return same as \code{x} or an error
#' @author Henjo de Knegt
#' @seealso \code{\link{taxon_id}}
#' @keywords internal
#' @examples \dontrun{
#' species_argcheck(camsample, species = "LowlandPaca")
#' species_argcheck(camsample, species = "LowlandPaca")
#' }
#' @export
species_argcheck <- function(x, species) {
  # input checks
  if(missing(x)) { stop("supply input x")}
  if(missing(species)) { stop("supply input species")}

  # check
  species <- tibble(taxonID = taxon_id(x, spp = species)) %>% 
    left_join(taxonomy(x), by = "taxonID") %>% 
    pull(vernacularNames.eng)

  # return
  return(species)
}