#' Check whether an object if of class ctdp
#' @param x an object
#' @return a \code{logical} whether or not the object is of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' is_ctdp(camsample)
#' }
#' @export
is_ctdp <- function(x) {
  "ctdp" %in% class(x)
}