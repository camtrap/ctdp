#' Retrieve the media table of a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @return a \code{tibble} with the media table
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' media(camsample)
#' }
#' @export
media <- function(x) {
  if(x$settings$media == FALSE){
    warning("media has been dropped from this object, thus returning the tibble with 0 rows")
  }
  y <- x$media
  return(y)
}