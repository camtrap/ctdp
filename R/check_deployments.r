#' @title Check integrity of deployments
#' @description Check integrity of deployments
#' @details NULL
#' @param x a \code{ctdp} object
#' @param duration_threshold the threshold duration, in days, above which deployments are flagged
#' @param sea_threshold the sun elevation angle, in degrees, above the horizon below which deployment start/end are flagged
#' @return a \code{tibble} with information columns:
#' \itemize{
#'   \item \code{deploymentID}: identifier
#'   \item \code{nr_false}: the number of validity check columns (see below) that contain value FALSE
#'   \item \code{deployment_interval}: interval
#'   \item \code{longitude,latitude}: lon/lat coordinates
#'   \item \code{locationName}: name
#'   \item \code{seq_start,seq_end}: timestamp of the first/last sequence in the deployment
#'   \item \code{duration}: duration (in days) of the deployment
#'   \item \code{sea_start,sea_end}: solar elevation angle (in degrees above horizon) at the time of start/end of deployment
#'   \item \code{dt_start,dt_end}: time difference (in days) between start/end of deployment and seq_start,seq_end
#' }
#' as well as validity check columns (prefix "ok_"):
#' \itemize{
#'   \item \code{start,end}: no NAs
#'   \item \code{startend}: end > start (thus length > 0 sec)
#'   \item \code{lon,lat}: no NAs
#'   \item \code{name}: no NAs
#'   \item \code{sea_start,sea_end}: start/end of deployment when sun is >= sea_threshold
#'   \item \code{duration}: difference between duration (days) <= duration_threshold
#' }
#' @author Henjo de Knegt
#' @seealso NULL
#' \dontrun{
#' test <- check_deployments(camsample)
#' test %>% filter(nr_false > 0)
#' }
#' @export
check_deployments <- function(x,
                              duration_threshold = 20*7, # days
                              sea_threshold = -6 # sun elevation angle in degrees above horizon
                              ) {
  # input checks
  if(missing(x)) { stop("supply input x")}

  # Force to interval
  x <- ctdp_interval(x)
  
  # Get data
  y <- x$deployments %>% 
    left_join(x$locations, by = "locationID") %>% 
    select(deploymentID,
           locationID,
           deployment_interval,
           longitude,
           latitude,
           locationName)
  
  # Get deployment_start/end
  x_int <- y$deployment_interval
  x_start <- int_start(x_int)
  x_end <- int_end(x_int)

  # Get sun elevation angle at deployment start and end
  spos_start <- solarpos(crds = cbind(y$longitude,
                                      y$latitude),
                         dateTime = with_tz(x_start, "UTC"))
  spos_end <- solarpos(crds = cbind(y$longitude,
                                    y$latitude),
                       dateTime = with_tz(x_end, "UTC"))
  spos <- tibble(start = x_start, 
                 e_start = spos_start[,2],
                 end = x_end, 
                 e_end = spos_end[,2])
  # spos
  if(FALSE) {
    plot(spos$start, spos$e_start, col = "2", pch = 16, ylim = range(c(spos$e_start, spos$e_end)) + c(0, 5),
         xlab = "deployment start", ylab = "solar elevation angle")
    points(spos$start, spos$e_end, col = 3, pch = 16)
    abline(h = 0)
    legend(x = "top", horiz = TRUE, bty = "n", pch=16, col = c(2,3), legend = c("start","end"))
  }
  
  # start/end according to sequences
  y_seqs_startend <- x$sequences %>% 
    group_by(deploymentID) %>% 
    summarise(seq_start = min(int_start(sequence_interval)),
              seq_end = max(int_end(sequence_interval)),
              .groups = "drop")
  
  # gather in summary tibble
  y_checks <- y %>% 
    left_join(y_seqs_startend, by = "deploymentID") %>% 
    mutate(
      duration = int_length(deployment_interval), # seconds
      duration = duration / 60 / 60 / 24, # days
      sea_start = spos$e_start,
      sea_end = spos$e_end,
      dt_start = as.numeric(difftime(int_start(deployment_interval),
                                     seq_start,
                                     units = "days")),
      dt_end = as.numeric(difftime(int_end(deployment_interval),
                                   seq_end,
                                   units = "days"))
    ) %>% 
    mutate(
      ok_start = !is.na(x_start),
      ok_end = !is.na(x_end),
      ok_startend = x_start < x_end,
      ok_lon = !is.na(longitude),
      ok_lat = !is.na(latitude),
      ok_name = !is.na(locationName),
      ok_sea_start = sea_start >= sea_threshold,
      ok_sea_end = sea_end >= sea_threshold,
      ok_duration = duration <= duration_threshold,
    )
  # glimpse(y_checks)

  # Summarize OK columns: how many are FALSE?
  y_false <- y_checks %>% 
    select(starts_with("ok_"))
  y_false <- ncol(y_false) - rowSums(y_false)
  y_checks <- y_checks %>% 
    mutate(nr_false = y_false) %>% 
    relocate(starts_with("ok_"), .after = deploymentID) %>% 
    relocate(nr_false, .after = deploymentID)
  # y_checks
  
  # Return
  return(y_checks)
}
