#' @title Get the offset in hours from UTC
#' @description Get the offset in hours from UTC
#' @param t an object of class \code{POSIXct}
#' @author Henjo de Knegt
#' @seealso NULL
#' @return a \code{numeric} with the offset (in hours) compared to UTC: negative is west of UTC.
#' @examples \dontrun{
#'   utc_offset(Sys.time())
#' }
#' @export
utc_offset <- function(t) {
  tutc <- force_tz(t, tzone = "UTC")
  tdiff <- as.numeric(difftime(tutc, t, units = "hours")) # tutc - t, as tutc is forced to utc
  return(tdiff)
}