#' Check whether an a ctdp object is NA
#' @inheritParams print.ctdp
#' @return a \code{logical} whether or not the object is of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' is.na(camsample)
#' }
#' @export
is.na.ctdp <- function(x) {
  FALSE
}