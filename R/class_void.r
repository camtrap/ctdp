#' Specify class
#' @param class optional \code{character} string with the class of species (e.g. "Mammalia") to focus analyses on, defaults to \code{NULL}
#' @return NULL
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @keywords internal
#' @export
class_void <- function(class = NULL) {
  NULL
}