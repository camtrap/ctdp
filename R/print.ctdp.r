#' @rdname ctdp
#' @param x an object of class \code{\link{ctdp}}
#' @export
print.ctdp <- function(x) {
  cat(paste0("ctdp datapackage which is a list with elements: \n"))
  for(i in seq_along(x)) {
    if(! is.data.frame(x[[i]])) {
      cat(" - ",names(x)[i], "\n")
    }
  }
  cat("\nand tables:\n")
  for(i in seq_along(x)) {
    if(is.data.frame(x[[i]])) {
      cat(" - ",names(x)[i], paste0("(n = ",formatC(nrow(x[[i]]), big.mark=","),")"), "\n")
    }
  }
  
  cat("\n")
  
  pathprops <- pathProperties(x$settings$path)
  if(pathprops$isZip) {
    cat("Zip file:",pathprops$fileFolder,"\n")
  }else {
    cat("Folder:",pathprops$fileFolder,"\n")
  }
  
  cat("Timezone:",x$settings$tz,"\n")
  cat("Interval:",x$settings$interval,"\n")
  cat("Primary keys:", paste(keycolNames(), collapse=", "),"\n")
  cat("Nr of stations:", nrow(x$locations),"\n")
  cat("Nr of species:", nrow(x$taxonomy),"\n")
  
  if(x$settings$media == FALSE) {
    cat("Media have been dropped\n")
  }
  
  tstart <- NULL
  tend <- NULL
  if(x$settings$interval == FALSE){
    x <- ctdp_interval(x, rev = FALSE)
  }
  if(nrow(x$deployments) > 0) {
    tstart <- x$deployments %>% pull(deployment_interval) %>% int_start() %>% min(na.rm = TRUE)
    tend   <- x$deployments %>% pull(deployment_interval) %>% int_end() %>% max(na.rm = TRUE)
  }
  
  cat("Time range:",
      ifelse(is.null(tstart),
             "",
             format(min(tstart), "%Y/%m/%d")),
      " -- ",
      ifelse(is.null(tend),
             "",
             format(max(tend), "%Y/%m/%d")),
      "\n")
}