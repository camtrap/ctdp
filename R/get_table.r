#' Retrieve a table from a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @param table \code{character} name of the table to retrieve: one of "locations", "deployments", "sequences", "observations", "media", "taxonomy" 
#' @return a \code{tibble} with the selected information table
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' get_table(camsample, table = "deployments")
#' }
#' @export
get_table <- function(x, table) {
  # checks
  if(missing(x)){stop("provide input x")}
  if(missing(table)){stop("provide input table")}
  table <- match.arg(table, choices = c("locations", "deployments", "sequences", "observations", "media", "taxonomy"))
  
  if(table == "media" & x$settings$media == FALSE) {
    warning("media has been dropped from this object, thus returning the tibble with 0 rows")
  }
  # Retrieve table
  y <- x[[table]]
  
  # Return
  return(y)
}