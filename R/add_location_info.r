#' Add metadata to camtrap locations
#' @param x an object of class \code{\link{ctdp}}
#' @param info a \code{tibble} with information to add to the locations of \code{x}. Will be matched to \code{x$locations} by \code{left_join}
#' @return x an object of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples NULL
#' @export
add_location_info <- function(x, info) {
  # checks
  if(missing(x)){stop("provide x")}
  if(missing(info)){stop("provide info")}
  if(! is.data.frame(info)){stop("provide info as a data.frame-like object")}
  
  # Get table with station properties
  y <- x$locations
  ynames <- names(y)
  
  # Any overlap in names?
  if(! any(names(info) %in% ynames)) { stop("no shared column to join data") }
  
  # Join via left_join
  z <- y %>%
    left_join(info)
  
  # check identical locationID
  if(nrow(y) != nrow(z)){stop("dimensions do not match")}
  if(!all(y$locationID == z$locationID)){stop("identifiers do not match")}
  
  # all okay, add to x
  x$locations <- z
  
  # return
  return(x)
}