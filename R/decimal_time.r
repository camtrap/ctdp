#' @title Convert a datetime object to decimal time
#' @description Convert a datetime object to decimal time
#' @param t an object of class \code{POSIXct}
#' @param units a \code{character} string with the units, defaults to \code{"hours"}, but can also be \code{"days"}
#' @author Henjo de Knegt
#' @seealso NULL
#' @return a \code{numeric} value with the decimal time
#' @examples \dontrun{
#'   decimal_time(Sys.time())
#'   decimal_time(Sys.time(), units="days")
#' }
#' @export
decimal_time <- function(t, units = "hours") {
  units <- match.arg(units, choices = c("hours","days"))
  t0 <- trunc.POSIXt(t, units = "days")
  y <- difftime(t, t0, units = units)
  y <- as.numeric(y)
  return(y)
}