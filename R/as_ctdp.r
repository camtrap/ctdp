#' @title Convert a camtraptor object to ctdp object
#' @description Convert a camtraptor object to ctdp object
#' @details Converts an object from the \code{camtraptor} package (see \url{https://inbo.github.io/camtraptor/} and \url{https://docs.ropensci.org/frictionless/}) to a \code{\link{ctdp}} object
#' @param x a \code{camtraptor} object
#' @param tz \code{character} string with time-zone code, defaults to \code{"Etc/GMT-2"}. See \code{\link{set_GMT_offset}}
#' @param verbose \code{logical}, defaults to \code{TRUE}: printing information on progress
#' @param rmEmpty \code{logical}, defaults to \code{TRUE}: remove columns that only contain NAs
#' @param pathInfo file path information internally called from \code{\link{read_ctdp}}
#' @param dropMedia \code{logical}, defaults to \code{FALSE}: whether or not to drop the media tibble.
#' @return an object of class \code{\link{ctdp}}.
#' @describeIn ctdp Convert a camtraptor object to ctdp object
#' @author Henjo de Knegt
#' @seealso \code{\link{read_ctdp}}, \code{\link{read_camtrap_dp}}
#' @examples NULL
#' @export
as_ctdp <- function(x,
                    tz = "Etc/GMT-2",
                    verbose = TRUE,
                    rmEmpty = FALSE,
                    pathInfo = NULL,
                    dropMedia = FALSE) {
  ### Checks
  if(missing(x)) { stop("supply input x")}

  
  ### Create copy of x: y
  y <- x
  
  
  ### Get pathInfo if NULL (supplied via read_ctdp)
  if(is.null(pathInfo)){
    pathInfo <- pathProperties(y$directory)
  }
  
  
  ### Check input y
  {
    ## Checks
    # names(y)
    strNames <- c("name","id","profile","created","sources","contributors","project",
                  "spatial","temporal","taxonomic","platform","resources","directory","data")
    
    nameMatch <- all(strNames %in% names(y))
    lengthMatch <- length(strNames) <= length(names(y))
    if(nameMatch == FALSE) { stop("inspect specification of data structure when loading from datapackage: names of list do not match") }
    if(lengthMatch == FALSE) { stop("inspect specification of data structure when loading from datapackage: length of returned list not as specified") }
    
    ## Exploring contents
    if(FALSE) {
      # Character properties
      y$name
      y$id
      y$profile
      y$created
      y$sources
      
      y$contributors
      y$organizations
      
      # Project settings
      y$project
      
      # Spatial-temporal delineation
      y$spatial
      y$temporal
      
      # Taxonomid info
      y$taxonomic
      
      y$platform
      
      # frictionless schema for data
      y$resources
      
      # Location of the file(s)
      y$directory
      
      # list with the 3 tibbles
      y$data
    }
  }

  
  if(verbose){ cat(" - post-processing\n") }
  
  
  ### Gather elements in list
  x <- list(taxonomy = list2tbl(y$taxonomic),
            deployments = y$data$deployments,
            observations = y$data$observations,
            media = y$data$media,
            is_interval = FALSE)

  
  ### Check column names
  # checkCodedColNames(x)
  x <- setCodedColNames(x)
  # printTimeZones(x)
  
  
  ### Remove 'timestamp' from observations if it is there:
  # it is the timestamp of the media (start) thus will be in sequence start/end/interval
  x$observations <- x$observations %>%
    select(-any_of("timestamp"))
  
  
  ### Get locations from deployments
  x[["locations"]] <- x$deployments %>%
    select(contains("location"),
           "longitude", "latitude") %>%
    distinct()

  
  ### Remove taxonomy columns from $observations (excl key columns)
  taxNames <- names(x$taxonomy)
  taxNames <- taxNames[! taxNames %in% as.character(unlist(keycolNames()))]
  taxNames <- c(taxNames,"deploymentID","mediaID")
  x$observations <- x$observations %>% select(-any_of(taxNames))
  
  
  ### Remove location columns from data$deployments (excl key columns)
  locNames <- names(x$locations)
  locNames <- locNames[! locNames %in% as.character(unlist(keycolNames()))]
  x$deployments <- x$deployments %>% 
    select(-any_of(locNames))


  ### Add taxonomic level (class and order) to each record in $taxonomy
  taxId <- function(y){ 
    if(is.na(y)){
      return(NA_character_)
    }else{
      return(pathProperties(y)$fileFolder)
    }
  }
  x$taxonomy$taxonID <- map_chr(x$taxonomy$taxonID, .f = taxId)
  x$observations$taxonID <- map_chr(x$observations$taxonID, .f = taxId)
  if(FALSE){
    for(i in x$taxonomy$taxonID){
      cat(i,":",taxon_level(i, level = "class"),"\n")
    }  
  }
  x$taxonomy$class <- unlist(sapply(x$taxonomy$taxonID, function(y){
    taxon_level(y, level = "class")
  }, simplify = TRUE, USE.NAMES = FALSE))
  # Already now in ctdp?
  if("order:" %in% names(x$taxonomy)){
    x$taxonomy <- x$taxonomy %>% 
      rename(order = 'order:')
  }else{
    x$taxonomy$order <- unlist(sapply(x$taxonomy$taxonID, function(y){
      taxon_level(y, level = "order")
    }, simplify = TRUE, USE.NAMES = FALSE))
  }
  # Rearrange
  x$taxonomy <- x$taxonomy %>% 
    select(scientificName, taxonID, taxonRank, class, order, family, everything())
  # x$taxonomy
  
  # Set "" to NA
  x$taxonomy <- x$taxonomy %>% 
    mutate(order = case_when(order == "" ~ NA_character_,
                             .default = order),
           family = case_when(family == "" ~ NA_character_,
                             .default = family))
  # x$taxonomy
  
  ### Make taxonRank an ordered factor
  x$taxonomy <- x$taxonomy %>% 
    mutate(taxonRank = factor(taxonRank,
                              levels = c("class","order","family","genus","species","subspecies"), 
                              ordered = TRUE))

  
  ### Remove fully empty columns
  if(rmEmpty) {
    x <- removeEmptyCols(x)
  }
  
  
  ### Set timezone to specified by tz for all dttm columns
  dttm_colNames <- dttmColNames(x)
  for(i in names(dttm_colNames)) {
    for(j in names(x[[i]])) {
      if("POSIXt" %in% class(x[[i]][[j]])) {
        x[[i]][[j]] <- with_tz(x[[i]][[j]], tzone = tz)
      }
    }  
  }
  # printTimeZones(x)
  
  
  ### Convert to interval objects, and standardize to positive length
  x$deployments <- x$deployments %>% 
    mutate(deployment_interval = interval(deployment_start, deployment_end),
           deployment_interval = int_standardize(deployment_interval)) %>% 
    relocate(deployment_interval, .before = deployment_start) %>% 
    select(-deployment_start, -deployment_end)
  x$is_interval <- TRUE
  # x$deployments$deployment_interval[[1]]
  # printTimeZones(x)
  

  ### Set camerasetup==TRUE to observationType=human (now "unclassified")
  # x$observations %>% count(observationType)
  # x$observations %>% count(cameraSetup)
  # x$observations %>% count(cameraSetup, observationType)
  # x$observations$observationType[x$observations$cameraSetup == TRUE] <- "human"
  # DETERMINE cameraSetupType:  setup, calibration or else NA
  # x$observations %>% select(observationType, cameraSetup, count, classificationMethod) %>% filter(cameraSetup == FALSE) %>% distinct() %>% as.data.frame()
  # observationType cameraSetup count classificationMethod
  # 1     unclassified       FALSE     1                human >> actually calibration
  # 2     unclassified       FALSE    NA                 <NA> >> REALLY unclassified
  x$observations <- x$observations %>% 
    mutate(cameraSetupType = 
             case_when(
               cameraSetup == TRUE ~ "setup",
               observationType == "unclassified" & classificationMethod == "human" ~ "calibration",
               TRUE ~ NA_character_
               ),
           cameraSetupType = factor(cameraSetupType, levels = c("calibration","setup")))
  
  
  ### Get sequences
  {
    if(verbose){ cat(" - retrieving sequences from media\n") }
    
    # data.table is considerably faster
    
    # Get data and convert to data.table with sequenceID as key
    sequences <- x$media %>% 
      distinct() %>% 
      select(deploymentID, sequenceID, media_timestamp, captureMethod) %>% 
      data.table(key = "sequenceID")
    
    # summarize per key
    sequences <- sequences[, list(deploymentID = unique(deploymentID),
                                  captureMethod = unique(captureMethod),
                                  start = min(media_timestamp),
                                  end = max(media_timestamp),
                                  nrphotos = length(media_timestamp)),
                           by = sequenceID]
    
    # convert to tibble, arrange, and convert start/end to interval object
    sequences <- sequences %>%
      as_tibble() %>%
      arrange(deploymentID, sequenceID) %>% 
      mutate(sequence_interval = interval(start, end)) %>% 
      relocate(sequence_interval, .before =  start) %>% 
      select(-start, -end)
  }
  # sequences
  
  
  ### Add to x
  x$sequences <- sequences
  
  
  ### Remove sequence columns from $media (excl key columns)
  seqNames <- names(x$sequences)
  seqNames <- seqNames[! seqNames %in% as.character(unlist(keycolNames()))]
  seqNames <- seqNames[seqNames != "deploymentID"]
  x$media <- x$media %>% 
    select(-any_of(seqNames))
  

  if(verbose){ cat(" - gathering data and returning\n") }
  
  
  ### Add settings and reorder elements
  x$settings = list(path = file.path(pathInfo$path,
                                     pathInfo$fileFolder),
                    tz = tz,
                    interval = TRUE,
                    media = !dropMedia)
  # names(x)
  x <- x[c("locations", "deployments", "sequences", "observations", "media", "taxonomy", "settings")]
  # names(x)
  
  
  ### UNIQUENESS OF NAMES (not key columns)
  # duplicateNames(x)
  x <- forceUniqueNames(x)
  # duplicateNames(x) %>% unlist() %>% as.character()
  
  
  ### Drop media?
  if(dropMedia) {
    x$media <- x$media %>% 
      slice_head(n = 1L)
    x$media <- x$media[-1,]
  }
  
  
  ### assign class ctdp
  class(x) <- c("ctdp","list")
  
  
  if(verbose){ cat(" - DONE\n") }
  
  
  
  #### FOR later development:
  # camtraptor info as element (hidden) in list or object (attribute)
  # with list y (data elements stripped to few rows?)
  # renaming info (hardcoded, duplicate names)
  # interval conversions
  
  ### Return
  return(x)
}