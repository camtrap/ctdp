#' @title Loading data in camptrap-dp format
#' @description This function loads a frictionless datapackage into an object of class \code{ctdp}, using the 'frictionless' and 'camtraptor' packages (see \url{https://inbo.github.io/camtraptor/} and \url{https://docs.ropensci.org/frictionless/}).
#' @details NULL
#' @param path \code{character} string with the path to the folder or zip-file holding the data
#' @return an object of class \code{ctdp}, which is a \code{list} with elements:
#' \itemize{
#'   \item \code{locations}: a \code{tibble} with info on camera locations
#'   \item \code{deployments}: a \code{tibble} with info on deployments
#'   \item \code{sequences}: a \code{tibble} with info on sequences
#'   \item \code{observations}: a \code{tibble} with info on observations
#'   \item \code{media}: a \code{tibble} with info on captured images
#'   \item \code{taxonomy}: a \code{tibble} with the used taxonomy table
#'   \item \code{settings}: a \code{list} with settings, including the path to the folder or file with source data, time zone, whether or not the object holds timestamps as \code{Interval} objects, and whether or not the object contains the media information.
#' }
#' @author Henjo de Knegt
#' @seealso \code{\link{as_ctdp}}
#' @examples \dontrun{
#'   data(camsample)
#'   camsample
#' }
#' @name ctdp
#' @aliases ctdp read_ctdp

#' @rdname ctdp
#' @export
read_ctdp <- function(path,
                      tz = "Etc/GMT-2",
                      verbose = TRUE,
                      rmEmpty = FALSE,
                      dropMedia = FALSE) {
  ### Checks
  if(missing(path)) { stop("supply input path")}

  
  ### Get file/folder info
  pathInfo <- pathProperties(path)
  if(verbose){ cat("Loading file",pathInfo$fileFolder,"\n") }
  
  
  ### unzip (if from zip)
  if(pathInfo$isZip) {
    if(verbose){ cat(" - unzipping file\n") }
    
    # create temp dir, unzip file into it
    tmpDir <- tempdir(check = TRUE)
    tmpfile <- tempfile(fileext = ".zip")
    file.copy(from = path, to = tmpfile)
    currentWD <- getwd()
    setwd(tmpDir)
    unzip(tmpfile, exdir=gsub(tmpfile, pattern=".zip", replacement=""))
    path <- gsub(tmpfile, pattern=".zip", replacement="")
    
    path <- gsub(path, pattern="\\\\", replacement="/")
    tmpfile <- gsub(tmpfile, pattern="\\\\", replacement="/")
    tmpDir <- gsub(tmpDir, pattern="\\\\", replacement="/")
    unzipped_folder <- gsub(tmpfile, pattern=".zip", replacement="")
    setwd(currentWD)
  }else{
    unzipped_folder <- file.path(pathInfo$path, pathInfo$fileFolder)
  }
  # unzipped_folder
  
  
  ### Load data into camtraptor object from unzipped_folder
  if(verbose){ cat(" - loading data\n") }
  {
    ## Checks on files
    fileNames <- list.files(unzipped_folder)
    if(all(c("datapackage.json","deployments.csv","media.csv","observations.csv") %in% fileNames) == FALSE){
      stop('the unzipped folder should contain the following files: "datapackage.json","deployments.csv","media.csv","observations.csv"')
    }

    ## Load from unzipped folder using camtraptor function
    y <- camtraptor::read_camtrap_dp(file = file.path(unzipped_folder,"datapackage.json"),
                                     media = TRUE)
  }

  
  ### Convert from camtraptor to ctdp
  x <- as_ctdp(y, 
               tz = tz,
               verbose = verbose,
               rmEmpty = rmEmpty,
               pathInfo = pathInfo,
               dropMedia = dropMedia)
  
  
  ### Return
  return(x)
}