#' Plot annotation status per deployment
#' @param x an object of class \code{\link{ctdp}}
#' @return a plot
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' plot_status(camsample)
#' plot_status(camsample, grouping = "transect")
#' }
#' @export
plot_status <- function(x,
                        IDcol = "locationName",
                        onlyMotion = TRUE,
                        xlab = "Time", ylab = "Location", 
                        
                        grouping = NULL,
                        dynamic = TRUE,
                        doPlot = TRUE) {
  # Get annotation status
  df <- fraction_annotated(x, onlyMotion = onlyMotion)
  
  # Add annotation fraction to x$deployments
  x$deployments <- x$deployments %>% 
    left_join(df, by = "deploymentID")

  # PLOT
  y <- plot_time_coverage(x,
                          IDcol = IDcol,
                          xlab = xlab, ylab = ylab, 
                          grouping = grouping,
                          colour = "fracAnnotated",
                          dynamic = dynamic,
                          addSequences = FALSE,
                          doPlot = doPlot)
  
  
  # invisibly return
  invisible(y)
}