#' @title Summarize (groups of) deployments
#' @description Summarize (groups of) deployments
#' @details NULL
#' @param x an object of class \code{\link{ctdp}}
#' @param datatable \code{logical}, whether or not to show as interactive datatable in the viewer, defaults to \code{FALSE}
#' @param onlyMotion \code{logical}, whether or not to only use motion detection images, defaults to \code{TRUE}
#' @param omitSetupType \code{logical}, whether or not to omit sequences that are marked as some form of setup (`observations` column `cameraSetupType` is than not NA), defaults to \code{TRUE}
#' @inheritParams filter_timerange
#' @inheritParams filter_station
#' @inheritParams by_void
#' @return a \code{tibble} like returned by \code{\link{fraction_annotated}}, but with extra column "effort", which contains the total number of camera-trap days for each group (row)
#' @author Henjo de Knegt
#' @seealso \code{\link{fraction_annotated}}
#' @examples \dontrun{
#' summarise_deployments(camsample)
#' summarise_deployments(camsample, by = "locationName")
#' summarise_deployments(camsample, by = "transect")
#' summarise_deployments(camsample, start = "2017/4/1", end = "2017/8/1")
#' summarise_deployments(camsample, by = "locationName", subset = str_sub(locationName, 5, 5) == "3") 
#' 
#' # As datatable in viewer
#' summarise_deployments(camsample, datatable = TRUE)
#' }
#' @export
summarise_deployments <- function(x,
                                  datatable = FALSE,
                                  onlyMotion = TRUE,
                                  omitSetupType = TRUE,
                                  
                                  start = NULL, end = NULL, orders = "%Y/%m/%d", # filter_timerange
                                  subset = NULL, # filter_station
                                  by = NULL # by_void
                                  ) {
  
  # Collect arguments
  allArgs <- as.list(match.call(expand.dots = FALSE))
  
  # filter time range?
  x <- filter_timerange(x, start = start, end = end, orders = orders)
  
  # filter on station?
  if(!is.null(allArgs$subset)){
    x <- do.call(filter_station,
                 allArgs[c("x","subset")],
                 quote = FALSE,
                 envir = parent.frame(n = 1L))
  }
  
  # Force interval
  x <- ctdp_interval(x)
  
  # annotation overview
  y1 <- fraction_annotated(x, onlyMotion = onlyMotion, omitSetupType = omitSetupType, by = NULL)
  
  # Add deployment length (days)
  y2 <- x$deployments %>% 
    select(deploymentID, deployment_interval) %>% 
    mutate(effort = int_length(deployment_interval) / 60 / 60 / 24) # DAYS
  
  # join y1 and y2
  y <- y2 %>% 
    left_join(y1, by = "deploymentID")
  # y
  
  ### Add by info?
  if(!is.null(by)){
    yby <- x$deployments %>% 
      left_join(x$locations, by = "locationID") %>% 
      select(all_of(unique(c("deploymentID",by))))
    y <- y %>% 
      left_join(yby, by = "deploymentID") %>% 
      relocate(all_of(by), .before = "deploymentID")
  }
  # y
  
  # Summarise by
  if(!is.null(by)){
    y <- y %>% 
      group_by(across(all_of(by))) %>% 
      summarise(effort = sum(effort),
                fracAnnotated = mean(fracAnnotated),
                nrSeqs = sum(nrSeqs),
                nrSeqsAnnotated = sum(nrSeqsAnnotated),
                nrPhotos = sum(nrPhotos),
                .groups = "drop") %>% 
      mutate(fracAnnotated = nrSeqsAnnotated / nrSeqs)
  }else{
    y <- y %>% 
      select(-deployment_interval)
  }
  # y

  # nrPhotos as integer
  y <- y %>% 
    mutate(nrPhotos = as.integer(nrPhotos))
  
  # show as datatable?
  if(datatable){
    y %>% 
      mutate(effort = round(effort, 3),
             fracAnnotated = round(fracAnnotated, 3)) %>% 
      datatable(caption = "Deployment summary",
                style = "auto")
  }
  
  # Return
  return(y)
}