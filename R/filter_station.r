#' Filter a ctdp object based on camera station properties
#' @param x an object of class \code{\link{ctdp}}
#' @param subset arguments used for filtering
#' @return x an object of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' locations(camsample)
#' str_sub(locations(camsample)$locationName, 5, 5)
#' filter_station(camsample, str_sub(locationName, 5, 5) == "3")
#' }
#' @export
filter_station <- function(x, subset = NULL) {
  # Get locations table
  xloc <- x$locations
  
  # Perform the filtering on it
  dofilter <- match.call(expand.dots = FALSE)$subset
  r <- eval(dofilter, xloc, parent.frame(1L))
  xsub <- xloc[r,]
  
  # Filter the rest of object x accordingly
  x$locations <- xsub
  x$deployments <- x$deployments %>%
    filter(locationID %in% xsub$locationID)
  x$sequences <- x$sequences %>%
    filter(deploymentID %in% x$deployments$deploymentID)
  x$observations <- x$observations %>%
    filter(sequenceID %in% x$sequences$sequenceID)
  x$media <- x$media %>%
    filter(sequenceID %in% x$sequences$sequenceID)
  
  # Return x
  return(x)
}