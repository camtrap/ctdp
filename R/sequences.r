#' Retrieve the sequences table of a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @return a \code{tibble} with the sequences table
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' sequences(camsample)
#' }
#' @export
sequences <- function(x) {
  y <- x$sequences
  return(y)
}