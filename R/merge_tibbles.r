#' @title Join the ctdp tibbles into 1 tibble with 1 row per observation
#' @param x an object of class \code{ctdp}
#' @param dropMedia \code{logical}, indicating whether or not to remove the \code{media} information. Defaults to \code{TRUE}
#' @param onlyDistinct \code{logical}, indicating whether or not to remove the duplicate rows. Defaults to \code{TRUE}
#' @param toInterval \code{logical}, indicating whether or not to return interval columns (for deployments and sequences start/end). Defaults to \code{TRUE}
#' @return a single object of class \code{tibble} with all data merged
#' @author Henjo de Knegt
#' @seealso \code{\link{nest_tibbles}}
#' @examples \dontrun{
#' merge_tibbles(camsample)
#' }
#' @export
merge_tibbles <- function(x, dropMedia = TRUE, onlyDistinct = TRUE, toInterval = TRUE) {
  
  # Force to interval?
  if(toInterval){
    x <- ctdp_interval(x)
  }else{
    x <- ctdp_interval(x, rev = TRUE)
  }
  
  # has media?
  if(has_media(x) == FALSE) {
    dropMedia <- TRUE
  }
  
  # Remove duplicates?
  if(onlyDistinct) { x <- distinct(x) }
  
  # Get keys and links
  keys <- keycolNames()
  linkStructure <- linkStructure()
  

  ### Get subsets of tibbles (minus the keys that should not be preserved)
  # y1 media
  # y2 sequences
  # y3 observations
  # y4 deployment
  # y5 locations
  # y6 taxonomy
  {
    omitKeys <- keys[-which(names(keys) %in% linkStructure$media)] %>% unlist() %>% as.character()
    y1 <- x$media %>% select(-any_of(omitKeys))
    omitKeys <- keys[-which(names(keys) %in% linkStructure$sequences)] %>% unlist() %>% as.character()
    y2 <- x$sequences %>% select(-any_of(omitKeys))
    omitKeys <- keys[-which(names(keys) %in% linkStructure$observations)] %>% unlist() %>% as.character()
    y3 <- x$observations %>% select(-any_of(omitKeys))
    omitKeys <- keys[-which(names(keys) %in% linkStructure$deployment)] %>% unlist() %>% as.character()
    y4 <- x$deployment %>% select(-any_of(omitKeys))
    omitKeys <- keys[-which(names(keys) %in% linkStructure$locations)] %>% unlist() %>% as.character()
    y5 <- x$locations %>% select(-any_of(omitKeys))
    omitKeys <- keys[-which(names(keys) %in% linkStructure$taxonomy)] %>% unlist() %>% as.character()
    y6 <- x$taxonomy %>% select(-any_of(omitKeys))
  }
  
  
  ### Get tibble with SEQUENCEID and MEDIA as list-column
  if(dropMedia == FALSE){
    # Get media data
    omitKeys <- keys[-which(names(keys) %in% linkStructure$media)] %>% unlist() %>% as.character()
    lc_media <- x$media %>%
      select(-any_of(omitKeys)) %>%
      select("sequenceID", everything()) %>%
      data.table(key = "sequenceID")
    lc_media <- lc_media %>%
      dt_nest(sequenceID, .key="media")
  }
  # lc_media
    
  
  ### Join deployments, observations, locations and taxonomy to sequences
  {
    # all with 2 columns: keys$sequences, and data (media, observations, deployments)
    # observations: y3, y6
    # deployments: y4, y5
    # media: y1
    
    y <- y2 %>%
      left_join(y3, by=keys$sequences, multiple = "all") %>%
      left_join(y4, by=keys$deployments) %>%
      left_join(y5, by=keys$locations) %>%
      left_join(y6, by=keys$taxonomy)
    # y

    # reorder columns: first keys, then rest
    y <- y %>%
      select(all_of(c(as.character(unlist(keys[which(names(keys) != "media")])))),
             everything())
    # y
  }
  # y
  
  
  ### Add media list column?
  if(dropMedia == FALSE) {
    y <- y %>%
      left_join(lc_media, by=keys$sequences) %>%
      relocate(media, .after = nrphotos)
  }
  

  ### Return
  return(y)
}