#' Specify filter expression
#' @param filter optional expression for subsetting, defaults to \code{NULL}
#' @return NULL
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @keywords internal
#' @export
filter_void <- function(filter = NULL) {
  NULL
}