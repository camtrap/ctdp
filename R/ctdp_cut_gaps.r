#' @title Cut long gaps in deployments
#' @description Cut long gaps in deployments
#' @param x an object of class \code{\link{ctdp}}
#' @param dtThreshold number of days, defaults to 14. An interval between consequtive sequences longer than this will break a deployment into separate parts.
#' @param rmSingledep \code{logical} defaults to TRUE: indicating whether or not deployments that consist of a single sequence should be removed
#' @return an object of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @export
cut_gaps <- function(x, # ctdp object
                     dtThreshold = 14, # days
                     rmSingledep = TRUE) { 
  ### Check for breaks (large gaps) in time between sequences
  seqMeta <- x$sequences %>% 
    arrange(deploymentID, int_start(sequence_interval)) %>% 
    group_by(deploymentID) %>% 
    mutate(dtPrev = as.numeric(difftime(int_start(sequence_interval), lag(int_start(sequence_interval)), units = "days")),
           dtPrev = replace_na(dtPrev, replace = -99),
           dtBurst = 1L + cumsum(as.integer(dtPrev > dtThreshold))) %>% 
    ungroup() %>% 
    select(sequenceID, deploymentID, sequence_interval, dtPrev,dtBurst)
  # seqMeta
  
  # Update to new deploymentID: oldID_burstNR
  seqMeta <- seqMeta %>% 
    group_by(deploymentID) %>% 
    mutate(nrBurst = length(unique(dtBurst))) %>% 
    ungroup() %>% 
    mutate(deploymentIDnew = case_when(nrBurst == 1 ~ deploymentID,
                                       nrBurst > 1 ~ str_c(deploymentID, dtBurst, sep="_burst")))
  # seqMeta
  # seqMeta %>% count(deploymentID, nrBurst)
  
  # new deployment ID/intervals
  depMeta <- seqMeta %>% 
    group_by(deploymentID, deploymentIDnew) %>% 
    summarise(start = min(int_start(sequence_interval)),
              end = max(int_start(sequence_interval)),
              .groups = "drop") %>% 
    mutate(deployment_intervalNEW = interval(start, end)) %>% 
    select(-start, -end)
  # depMeta
  
  # Update object x
  y <- x
  y$deployments <- y$deployments %>% 
    left_join(depMeta, by = "deploymentID") %>% 
    mutate(deploymentID = deploymentIDnew,
           deployment_interval = deployment_intervalNEW) %>% 
    select(-deploymentIDnew, -deployment_intervalNEW)
  y$sequences <- y$sequences %>% 
    left_join(select(seqMeta, sequenceID, deploymentIDnew), by = "sequenceID") %>% 
    mutate(deploymentID = deploymentIDnew) %>% 
    select(-deploymentIDnew)
  
  # Remove deployments for which there now is just 1 sequence?
  if(rmSingledep){
    y$deployments <- y$deployments %>% 
      filter(int_length(deployment_interval) > 1)
    y$sequences <- y$sequences %>% 
      filter(deploymentID %in% y$deployments$deploymentID)
    y$locations <- y$locations %>% 
      filter(locationID %in% y$deployments$locationID)
    y$observations <- y$observations %>% 
      filter(sequenceID %in% y$sequences$sequenceID)
    y$media <- y$media %>% 
      filter(sequenceID %in% y$sequences$sequenceID)
  }
  
  # Return
  return(y)
}