#' @title title
#' @description title
#' @details NULL
#' @param x an object of class \code{ctdp}
#' @param doTimelapse whether or not to include time-lapse sequences in the analyses, defaults to \code{FALSE}
#' @param doUnclassified whether or not to include unclassified sequences in the analyses, defaults to \code{TRUE}
#' @param doHuman whether or not to include humans in the analyses, defaults to \code{TRUE}
#' @inheritParams class_void
#' @inheritParams species_void
#' @param doPlot \code{logical} whether or not to plot the results, defaults to \code{TRUE}
#' @param byTaxon \code{logical} whether or not to fit an activity pattern per taxon, defaults to \code{TRUE}
#' @param n_min \code{integer,numeric} minimum number of records for activity pattern to be fitted, defaults to 25
#' @inheritParams filter_timerange
#' @inheritParams filter_station
#' @inheritParams by_void
#' @return invisibly returns a nested tibble with the fitted activity patterns per group
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' activity_ctdp(camsample)
#' activity_ctdp(camsample, class = "Mammalia")
#' activity_ctdp(camsample, species = c("CollaredPeccary","OCELOT"))
#' activity_ctdp(camsample, species = c("CollaredPeccary","OCELOT"), by = "locationName")
#' activity_ctdp(camsample, by = "locationName", subset = str_sub(locationName, 5, 5) == "3")
#'
#' test <- activity_ctdp(camsample, by = "locationName", subset = str_sub(locationName, 5, 5) == "3")
#' test
#' }
#' @export
activity_ctdp <- function(x,
                          doTimelapse = FALSE,
                          doUnclassified = TRUE,
                          doHuman = TRUE,
                          
                          class = NULL,
                          species = NULL,
                          
                          doPlot = TRUE,
                          byTaxon = TRUE,
                          n_min = 25,
                          
                          start = NULL, end = NULL, orders = "%Y/%m/%d", # filter_timerange
                          subset = NULL, # filter_station
                          by = NULL # by_void
                          ) {
  # Collect arguments
  allArgs <- as.list(match.call(expand.dots = FALSE))
  
  # filter time range?
  x <- filter_timerange(x, start = start, end = end, orders = orders)
  
  # filter on station?
  if(!is.null(allArgs$subset)){
    x <- do.call(filter_station,
                 allArgs[c("x","subset")],
                 quote = FALSE,
                 envir = parent.frame(n = 1L))
  }
  
  # Get data
  y <- merge_tibbles(x)
  
  # filter out time lapse?
  # y %>% count(captureMethod)
  if(doTimelapse == FALSE) {
    y <- y %>% 
      filter(captureMethod != "time lapse")  
  }
  
  # remove blanks
  # y %>% count(observationType)
  y <- y %>% filter(observationType != "blank")
  
  # Filter out unclassified?
  if(doUnclassified == FALSE) {
    y <- y %>% 
      filter(observationType != "unclassified")
  }
  
  # Filter out human?
  if(doHuman == FALSE) {
    y <- y %>% 
      filter(observationType != "human")
  }
  
  # Filter to class?
  # y %>% count(class)
  if(!is.null(class)){
    doClass <- class_argcheck(x, class)
    y <- y %>% 
      filter(class == doClass | is.na(class))
  }
  # y %>% count(class)
  
  # Filter to species?
  if(!is.null(species)){
    doSpecies <- taxon_id(x, species)
    y <- y %>% 
      filter(observationType != "animal" | 
               (observationType == "animal" & taxonID %in% doSpecies))
    
  }
  # y %>% count(observationType, taxonID)
  
  # Add label (based on byTaxon)
  if(byTaxon){
    y <- y %>% 
      mutate(label = case_when(is.na(vernacularNames.eng) ~ as.character(observationType),
                               TRUE ~ vernacularNames.eng))
  }else{
    y <- y %>% 
      mutate(label = as.character(observationType))
  }
  # y %>% count(label)
  
  # combine label and byLabel
  if(!is.null(by)){
    y <- y %>% 
      rowwise() %>%
      mutate(byLabel = paste(c_across(all_of(by)), collapse = "_")) %>% 
      mutate(label = str_c(byLabel, label, sep = "_")) %>% 
      ungroup() %>% 
      select(-byLabel)
  }
  # y %>% count(label)

  # label to factor and arrange
  # y %>% count(observationType)
  y <- y %>% 
    arrange(desc(observationType), vernacularNames.eng)
  y <- y %>% mutate(label = factor(label, levels = unique(label)))
  # y %>% count(label)
  # y
  
  # calculate solar time 
  ysolar <- solartime(dat = int_start(y$sequence_interval),
                      lat = y$latitude,
                      lon = y$longitude,
                      tz = as.numeric(utc_offset(int_start(y$sequence_interval))))
  
  # add to y and nest per observationType / vernacularNames.eng
  y$solartime <- ysolar$solar
  
  # Nest to each group
  selectCols <- c("observationType", "vernacularNames.eng", "solartime", "label", by)
  groupCols <- selectCols[selectCols != "solartime"]
  y <- y %>% 
    select(all_of(selectCols)) %>% 
    group_by(across(all_of(groupCols))) %>% 
    nest() %>% 
    ungroup()
  
  # Check sample size and filter to only those >= n_min
  y <- y %>% 
    mutate(n = map_int(data, nrow)) %>% 
    filter(n >= n_min) %>% 
    select(-n)
  
  # Fit act for each group/row
  y <- y %>% 
    mutate(fact = map(data, function(z){fitact(z$solartime)}))
  # y
  # plot(y$fact[[1]])

  # plot
  if(doPlot){
    # Get maximum density
    maxDens <- numeric(length(y$fact))
    for(i in seq_along(y$fact)){
      maxDens[[i]] <- max(y$fact[[i]]@pdf[,2], na.rm=TRUE)
    }
    maxDens <- max(maxDens) * (2*pi) / 24 # correction radians >> 24hr clock
    for(i in seq_along(y$fact)){
      if(i == 1){
        plot(y$fact[[i]], xunit = "clock", yunit = "density", data = "none", ylim = c(0, 1.1*maxDens))
      }else{
        plot(y$fact[[i]], xunit = "clock", yunit = "density", data = "none", add = TRUE, tline=list(col=i))
      }
    }
    legend(x = "topleft", horiz=FALSE, bty="n", lty = 1, col = seq_along(y$fact), 
           legend = y$label, cex = 0.75, x.intersp = 0.2, text.width = 2)
  }

  # insivibly return
  invisible(y)
}
