#' Get captures
#' @param x an object of class \code{\link{ctdp}}
#' @param onlyAnimal \code{logical}, defaults to \code{TRUE}, indicating whether or not to only focus on \code{observationType == "animal"}
#' @param class \code{character} string with the class of species to compute the captures for (e.g. "Mammalia"), defaults to NULL
#' @inheritParams species_void
#' @inheritParams filter_timerange
#' @inheritParams filter_station
#' @inheritParams by_void
#' @return a \code{tibble} with counts per species (with columns added: "observationType","class","order", as well as the columns specified in \code{by}).
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}},\code{\link{taxon_id}},  \code{\link{filter_timerange}}
#' @examples \dontrun{
#' captures(camsample)
#' captures(camsample, class = "Mammalia")
#' captures(camsample, species = c("CollaredPeccary","OCELOT"))
#' captures(camsample, by = "locationName")
#' captures(camsample, species = c("CollaredPeccary","OCELOT"), by = "locationName")
#' captures(camsample, start = "2017/4/1", end = "2017/8/1")
#' captures(camsample, by = "locationName", subset = str_sub(locationName, 5, 5) == "3")
#' }
#' @export
captures <- function(x,
                     
                     onlyAnimal = TRUE,
                     class = NULL, # class_void
                     species = NULL, # species_void
                     
                     start = NULL, end = NULL, orders = "%Y/%m/%d", # filter_timerange
                     subset = NULL, # filter_station
                     by = NULL # by_void
                     ) {
  # Collect arguments
  allArgs <- as.list(match.call(expand.dots = FALSE))

  # filter time range?
  x <- filter_timerange(x, start = start, end = end, orders = orders)
 
  # filter on station?
  if(!is.null(allArgs$subset)){
    x <- do.call(filter_station,
                 allArgs[c("x","subset")],
                 quote = FALSE,
                 envir = parent.frame(n = 1L))
  }

  # compute effort table (only "by" - rest has been dealt with)
  if(!is.null(by)){
    effortTable <- calc_effort(x, by = by)
  }else{
    effortTable <- calc_effort(x)
  }

  # Merge tibbles
  y <- merge_tibbles(x, dropMedia = TRUE)
  
  # only animals?
  if(onlyAnimal) {
    y <- y %>% 
      filter(observationType == "animal")
  }

  # for specific class?
  if(!is.null(class)){
    doClass <- class_argcheck(x, class); rm(class)  
    y <- y %>% 
      filter(class == doClass)
  }
  
  # Focus on specific species?
  if(!is.null(species)){
    doSpecies <- species_argcheck(x, species); rm(species)
    doTaxonIDs <- taxon_id(x, doSpecies)
    y <- y %>% 
      filter(taxonID %in% doTaxonIDs)
  }
  
  # select columns
  keepCols <- c("taxonID", "captureMethod", "observationType", "count", "classificationMethod",
                "scientificName", "vernacularNames.eng", "class", "order", # "vernacularNames.nl"
                by) %>% 
    unique()
  y <- y %>% 
    select(all_of(keepCols))
  
  # Specify grouping
  y <- y %>% 
    group_by(pick(all_of(unique(c("observationType","class","order", by)))))
  
  # Count
  z <- y %>% 
    count(taxonID, vernacularNames.eng) %>% 
    ungroup() %>% 
    rename(captures = n)
  
  # Add effort
  if(!is.null(by)){
    z <- z %>% 
      left_join(effortTable, by = by)
  }else{
    z <- z %>% 
      mutate(effort = effortTable$effort[1])
  }
  z <- z %>% 
    mutate(capture_rate = captures / effort)

  # return
  return(z)
}