#' Check whether a ctdp object has media information
#' @inheritParams print.ctdp
#' @return a \code{character} string with the time zone information of the date-time objects in \code{x}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' has_media(camsample)
#' }
#' @export
has_media <- function(x) {
  x$settings$media
}