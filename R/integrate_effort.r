#' Integrate effort over a specified time span
#' @param x an object of class \code{\link{ctdp}}
#' @inheritParams filter_timerange
#' @inheritParams filter_station
#' @return a \code{tibble} with effort data
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}, \code{\link{filter_station}}
#' @examples \code{
#' integrate_effort(camsample)
#' integrate_effort(camsample, start = "2017/4/1", end = "2017/8/1")
#' integrate_effort(camsample, subset = str_sub(locationName, 5, 5) == "3")
#' integrate_effort(camsample,
#'   start = "2017/4/1", end = "2017/8/1", 
#'   subset = str_sub(locationName, 5, 5) == "3")
#' }
#' @export
integrate_effort <- function(x, 
                             start = NULL, end = NULL, orders = "%Y/%m/%d",
                             subset = NULL) {
  # checks
  if(missing(x)){stop("provide input x")}
  
  # filter time range?
  x <- filter_timerange(x, start = start, end = end, orders = orders)
  
  # filter on station?
  argList <- as.list(match.call(expand.dots = FALSE))[c("x","subset")]
  if(!is.null(argList$subset)){
    x <- do.call(filter_station,
                 argList,
                 quote = FALSE,
                 envir = parent.frame(n = 1L))
  }

  # Get table with effort data
  y <- effort_table(x, startend = TRUE)
  
  # Get/set start/end
  if(is.null(start)){
    start <- head(y$time, 1)
  }else{
    if(is.character(start)){
      start <- parse_date_time(start, orders = orders, tz = x$settings$tz)
    }
  }
  if(is.null(end)){
    end <- tail(y$time, 1)
  }else{
    if(is.character(end)){
      end <- parse_date_time(end, orders = orders, tz = x$settings$tz)
    }
  }
  # start
  # end
  
  # Truncate
  ystart <- max(which(y$time <= start))
  yend   <- min(which(y$time >= end))
  # y$time[c(ystart, yend)]
  z <- y[ystart:yend,]
  z$time[1] <- start
  z$time[nrow(z)] <- end
  # z
  
  # compute total effort as integration (i.e. area under curve)
  totalEffort <- z %>% 
    mutate(timeNext = lead(time, n = 1L),
           duration = as.numeric(difftime(timeNext, time, units = "days")),
           duration = replace_na(duration, replace = 0.0),
           prod = nrCams * duration) %>% 
    pull(prod) %>% 
    sum()
  
  # return
  return(totalEffort)
}