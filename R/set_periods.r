#' @title Specify time periods
#' @description Specify time periods
#' @param xxx still to write
#' @author Henjo de Knegt
#' @seealso NULL
#' @return xxx still to write
#' @examples \dontrun{
#' # Either using start/end timestamps (in format POSIXct)
#' start <- seq(from = ISOdatetime(2022,11,1,0,0,0,tz="Etc/GMT-2"), 
#'              to = ISOdatetime(2022,12,7,0,0,0,tz="Etc/GMT-2"),
#'              by = "2 weeks")
#' set_periods(start = start, 
#'             end = start + period(2, units = "weeks"),
#'             name = paste0("p", seq_along(start)))
#'             
#' # OR: using vector of years, and start/ends are character vectors (recurrent within each year)       
#' set_periods(start = c("4-21 08:00", "6-21 08:00"),
#'             end = c("5-19 18:00", "7-19 18:00"), 
#'             orders = "md HM",
#'             years = 2017:2020,
#'             tz = "Etc/GMT+5")
#' }
#' @export
set_periods <- function(start, end,
                        name = NULL,
                        years = NULL,
                        orders = "md HM", 
                        tz = NULL) {
  # Checks
  if(missing(start)){stop("supply start")}
  if(missing(end)){stop("supply end")}
  n <- length(start)
  if(length(end) != n){stop("length of start and end should be equal")}
  if(!is.null(name)){
    if(length(name) != n){stop("when supplying name, it should equal the length of start and end")}
  }
  
  # Overwrite name (if NULL)
  if(is.null(name)){
    name <- paste0("p", seq_along(start))
  }

  # Supplying years, thus start and end are character (with orders and tz), then expand grid
  if(!is.null(years)){
    # start and end are posixct
    if(!is.character(start)){stop("start should be of class character (when input to years is supplied")}
    if(!is.character(end)){stop("end should be of class character (when input to years is supplied")}
    if(missing(tz)){stop("supply tz")}
    if(str_detect(tolower(orders), pattern = "y")){stop("orders (and thus also start and end) should not include a year identifier (y or Y)")}
    ordersFull <- str_c("Y", orders, sep = " ")
    
    # expand grid
    ints <- tibble(year = rep(years, each = length(start)), 
                   start = rep(start, times = length(years)),
                   end = rep(end, times = length(years)),
                   name = rep(name, times = length(years))) %>% 
      as_tibble() %>% 
      mutate(start = str_c(year, start, sep = "-"),
             end = str_c(year, end, sep = "-"),
             start = parse_date_time(start, orders = ordersFull, tz = tz),
             end = parse_date_time(end, orders = ordersFull, tz = tz),
             interval = interval(start = start, end = end),
             name = str_c(year, name, sep = "_"))
    
    # Overwrite
    start <- ints$start
    end <- ints$end
    name <- ints$name
  }else{
    # start and end are POSIXct
    if(!is.POSIXct(start)){stop("start should be of class POSIXct (when no input to years is supplied")}
    if(!is.POSIXct(end)){stop("end should be of class POSIXct (when no input to years is supplied")}
  }
  
  # merge into tibble
  prds <- tibble(name = name,
                 start = start,
                 end = end) %>% 
    mutate(interval = interval(start, end))
  
  # Return
  return(select(prds, name, interval))
}