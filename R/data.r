#' @title The main columns of a ctdp object
#' @author Henjo de Knegt
#' @seealso NULL
#' @docType data
#' @keywords datasets
#' @name ctdp_mainCols
#' @usage NULL
#' @format \code{tibble} with the main columns of a \code{ctdp} object, per table.
NULL

#' @title Example ctdp dataset
#' @author Henjo de Knegt
#' @seealso NULL
#' @docType data
#' @keywords datasets
#' @name bcnm2017
#' @usage NULL
#' @format an object of class \code{ctdp} with the observations from 6 camera stations at the Barro Colorado Nature Monument (BCNM) during 2017.
NULL
