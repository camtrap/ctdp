#' Filter a ctdp object based on deployment properties
#' @param x an object of class \code{\link{ctdp}}
#' @param subset arguments used for filtering
#' @return x an object of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' # Just to plug in some dummy data
#' y <- camsample
#' y$habitat <- 1:42
#' deployments(y)
#' filter_deployments(y, habitat > 10)
#' }
#' @export
filter_deployments <- function(x, subset = NULL) {
  # Get deployment table
  xdep <- x$deployments
  
  # Perform the filtering on it
  dofilter <- match.call(expand.dots = FALSE)$subset
  r <- eval(dofilter, xdep, parent.frame(1L))
  xsub <- xdep[r,]
  
  # Filter the rest of object x accordingly
  x$deployments <- xsub
  x$locations <- x$locations %>%
    filter(locationID %in% xsub$locationID)
  x$sequences <- x$sequences %>%
    filter(deploymentID %in% x$deployments$deploymentID)
  x$observations <- x$observations %>%
    filter(sequenceID %in% x$sequences$sequenceID)
  x$media <- x$media %>%
    filter(sequenceID %in% x$sequences$sequenceID)
  
  # Return x
  return(x)
}