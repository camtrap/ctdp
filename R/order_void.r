#' Specify order
#' @param order optional \code{character} string with the order of species (e.g. "Carnivora") to focus analyses on, defaults to \code{NULL}
#' @return NULL
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @keywords internal
#' @export
order_void <- function(order = NULL) {
  NULL
}