#' @title drop columns that only contain NAs
#' @description drop columns that only contain NAs
#' @param x an object of class \code{ctdp}
#' @return an object of class \code{ctdp}
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' drop_na_cols(camsample)
#' }
#' @export
drop_na_cols <- function(x) {
  x <- removeEmptyCols(x, keepMainCols = TRUE)
  
  # return
  return(x)
}