#' Retrieve the time range of a ctdp object
#' @param x an object of class \code{\link{ctdp}}
#' @param based_on \code{character} vector based on which element the time range should be computed: one of "deployments" (default), "sequences", "media".
#' @return a \code{dttm} vector of length 2 with the start/end of the ctdp object
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' time_range(camsample)
#' }
#' @export
time_range <- function(x, based_on = c("deployments","sequences","media")[1]) {
  # Prep
  based_on <- match.arg(based_on, choices = c("deployments","sequences","media"))
  x <- ctdp_interval(x)
  
  # Get times
  if(based_on == "deployments"){
    times <- x$deployments$deployment_interval
    starts <- int_start(times)
    ends   <- int_end(times)
  }
  if(based_on == "sequences"){
    times <- x$sequences$sequence_interval
    starts <- int_start(times)
    ends   <- int_end(times)
  }
  if(based_on == "media"){
    times <- x$media$media_timestamp
    starts <- int_start(times)
    ends   <- int_end(times)
  }
  
  # Get range
  trange <- range(starts)
  trange[2] <- max(ends)
  
  # Return
  return(trange)
}