#' @title Conversion between start/end and Interval objects
#' @description Convert between start/end and Interval objects for deployments and sequences
#' @param x an object of class \code{\link{ctdp}}
#' @param rev \code{logical} indicating to reverse the conversion: thus when \code{TRUE} the interval object is converted back to start/end
#' @return an object of class \code{\link{ctdp}}
#' @author Henjo de Knegt
#' @seealso \code{\link{ctdp}}
#' @examples \dontrun{
#' ctdp_interval(camsample)
#' ctdp_interval(camsample, rev = TRUE)
#' }
#' @export
ctdp_interval <- function(x, rev = FALSE) {
  if(x$settings$interval) {
    if(rev){
      # deployment_interval >> deployment_start/deployment_end
      x$deployments <- x$deployments %>% 
        mutate(deployment_start = int_start(deployment_interval),
               deployment_end   = int_end(deployment_interval)) %>%
        relocate(deployment_start:deployment_end, .before = deployment_interval) %>% 
        select(-deployment_interval)
      # sequence_interval >> sequence_start/deployment_end
      x$sequences <- x$sequences %>% 
        mutate(sequence_start = int_start(sequence_interval),
               sequence_end   = int_end(sequence_interval)) %>%
        relocate(sequence_start:sequence_end, .before = sequence_interval) %>% 
        select(-sequence_interval)
      # Update settings
      x$settings$interval <- FALSE
    }
  }else{
    if(rev == FALSE){
      # deployment_start/deployment_end >> deployment_interval
      x$deployments <- x$deployments %>% 
        mutate(deployment_interval = interval(deployment_start, deployment_end)) %>%
        relocate(deployment_interval, .before = deployment_start) %>% 
        select(-deployment_start, deployment_end)
      # sequence_start/deployment_end >> sequence_interval
      x$sequences <- x$sequences %>% 
        mutate(sequence_interval = interval(sequence_start, sequence_end)) %>%
        relocate(sequence_interval, .before = sequence_start) %>% 
        select(-sequence_start, sequence_end)
      # Update settings
      x$settings$interval <- TRUE
    }
  }

  ### return
  return(x)
}