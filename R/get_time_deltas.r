#' @title Compute time between deployments and start/end sequences deployments
#' @description Compute time between deployments and start/end sequences deployments
#' @details NULL
#' @param x a \code{ctdp} object
#' @param units_delta a \code{character} string with units of time difference (defaults to "mins")
#' @param units_duration a \code{character} string with units of time difference (defaults to "days")
#' @return a \code{tibble} object with ...
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @export
get_time_deltas <- function(x, units_delta = "mins", units_duration = "days"){
  # force to interval
  x <- ctdp_interval(x)
  
  # get deployment info
  xdep <- x$deployments %>% 
    select(deploymentID, locationID, deployment_interval) %>% 
    mutate(rownumorg = row_number(),
           start = int_start(deployment_interval),
           end = int_end(deployment_interval),
           duration = as.numeric(difftime(end, start, units = "days"))) %>% 
    arrange(locationID, start) %>% 
    mutate(rownum = row_number())
  # xdep
  
  # Compute dtime between start current and end prev depl
  xdep <- xdep %>% 
    group_by(locationID) %>% 
    mutate(tdiff_prev = as.numeric(difftime(start, lag(end), units = "mins"))) %>% 
    ungroup()
  # xdep
  # hist(xdep$tdiff)
  # xdep %>% arrange(tdiff)
  # xdep %>% filter(deploymentID == "fd292cf3-7960-4615-ac16-96c82c906a65")

  # add deltatime between first/last sequence and start/end deployment interval
  xseq <- x$sequences %>%
    select(deploymentID, sequence_interval) %>% 
    mutate(start = int_start(sequence_interval),
           end = int_end(sequence_interval)) %>% 
    select(-sequence_interval) %>% 
    group_by(deploymentID) %>% 
    summarise(start_seq = min(start),
              end_seq = max(end))
  xdep <- xdep %>% 
    left_join(xseq, by = "deploymentID")
  # compute tdiff between start dep and start_seq (and idem end)
  xdep <- xdep %>% 
    mutate(tdiff_start = as.numeric(difftime(start, start_seq, units = "mins")),
           tdiff_end = as.numeric(difftime(end, end_seq, units = "mins")))
  # xdep
  
  # get data to return
  xdep <- xdep %>% 
    select(deploymentID, deployment_interval, tdiff_prev, tdiff_start, tdiff_end)
  
  # return
  return(xdep)
}