#' Specify grouping columns
#' @param by an optional \code{character} vector specifying the column names by which to group analyses, defaults to \code{NULL}
#' @return NULL
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples NULL
#' @keywords internal
#' @export
by_void <- function(by = NULL) {
  NULL
}