#' Retrieve the column names of the ctdp tibbles
#' @inheritParams print.ctdp
#' @param verbose \code{logical}, defaults to \code{TRUE}, indicating whether or not to print information to the console
#' @return a named \code{list} with the names of the tibbles (returned invisibly when \code{verbose=TRUE})
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' column_names(camsample)
#'   
#' xnames <- column_names(camsample, verbose = FALSE)
#' xnames
#' }
#' @export
column_names <- function(x, verbose=TRUE) {
  y <- list()
  ynames <- names(x)
  for(i in seq_len(length(x)-1))
  {
    if(verbose){cat(paste0("\n\nTable $", ynames[i], ":\n"))}
    y[[ynames[i]]] <- names(x[[i]])
    if(verbose){cat(paste0(" - ",paste(y[[ynames[i]]], collapse="\n - ")))}
  }
  if(verbose)
  {
    invisible(y)
  }else
  {
    return(y)
  }
}