#' @title drop the media slot of a ctdp object
#' @description drop the media slot of a ctdp object
#' @param x an object of class \code{ctdp}
#' @return NULL
#' @author Henjo de Knegt
#' @seealso NULL
#' @examples \dontrun{
#' drop_media(camsample)
#' }
#' @export
drop_media <- function(x) {
  # drop media element
  x$media <- x$media %>% 
    slice_head(n = 1L)
  x$media <- x$media[-1,]
  
  # update settings
  x$settings$media <- FALSE
  
  # return
  return(x)
}